/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;

/**
 *
 * @author anderson
 */
public class BuscaCodigoProjeto {

    private static final String RESULT = "result";
    private static final String CONTENT = "content";
    private static final String OFFSET = "#FIM#";
    private final static String URL = "https://api.topcoder.com/v3/challenges/?filter=status%3DCompleted&limit=100&orderBy=submissionEndDate&offset=" + OFFSET;

    public static void busca() throws IOException {

        for (int x = 1; x <= 100000; x++) {
            JsonObject enviarHttpGet = HttpUtil.enviarHttpGet(URL.replace(OFFSET, "" + (x * 100)));

            JsonArray resp = enviarHttpGet.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray();

            for (JsonElement json : resp) {
                String asString = json.getAsJsonObject().get("id").getAsString();
                System.out.println("\"" + asString + "\",");
            }

            

        }
    }

}

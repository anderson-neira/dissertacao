/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.DataUtil;
import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.DetalheMembroMaratona;
import br.uem.tccrawler.model.Maratona;
import br.uem.tccrawler.model.MaratonaRound;
import br.uem.tccrawler.model.Membro;
import br.uem.tccrawler.model.MembroMaratona;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class MaratonaService {

    private static final String REMOVER = "REMOVER";
    private static final String RESULT = "result";
    private static final String CONTENT = "content";
    private static final String LIMIT = "LIMIT";
    private static final String OFFSET = "OFFSET";
    private static final String METADATA = "metadata";

    private static final String SMRURL = "https://api.topcoder.com/v3/members/" + REMOVER + "/mms/?filter=track%3dDEVELOP&limit=" + LIMIT + "&offset=" + OFFSET + "&orderBy=endDate+desc";

    public static Membro buscaMaratona(Membro entidade) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(SMRURL.replace(OFFSET, "0").replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
            entidade.setQtdeMaratonas(resp.get(RESULT).getAsJsonObject().get(METADATA).getAsJsonObject().get("totalCount").getAsInt());

            // tem dados para salvar
            if (entidade.getQtdeMaratonas() > 0) {
                // não precisa de mais de uma requisição
                if (entidade.getQtdeMaratonas() <= 50) {
                    entidade = geraMaratona(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                } else {
                    // pega os desafios da primeira requisição
                    entidade = geraMaratona(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);

                    // precisa de mais de uma reqsuicação                   
                    for (int x = 1; x < entidade.getQtdeMaratonas() / 50; x++) {
                        //pega a proxima pagina de desafios
                        JsonObject respMeio = HttpUtil.enviarHttpGet(SMRURL.replace(OFFSET, "" + 50 * x).replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
                        // para cada desafio cria um obj
                        entidade = geraMaratona(respMeio.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                    }
                    // pega os ultimos dados                     
                    String lim = entidade.getQtdeDesafiosDesenvolvimento() % 50 == 0 ? "50" : "" + entidade.getQtdeDesafiosDesenvolvimento() % 50;
                    JsonObject respUltima = HttpUtil.enviarHttpGet(SMRURL.replace(OFFSET, "" + (entidade.getQtdeMaratonas() / 50) * 50).replace(REMOVER, entidade.getNome()).replace(LIMIT, lim));
                    entidade = geraMaratona(respUltima.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entidade;
    }

    private static Membro geraMaratona(JsonArray array, Membro entidade) {
        for (JsonElement obj : array) {

            Maratona maratona = buscaPorIdTopCoder(obj);

            MembroMaratona membroMaratona = new MembroMaratona(entidade, maratona);
            membroMaratona.setDetalhesMembroMaratona(gerarDetalhes(obj, membroMaratona));

            entidade.getMembrosMaratona().add(membroMaratona);
        }
        return entidade;
    }

    /**
     * Este método busca a maratona pelo id do topCoder, caso não esteja
     * cadastrado ele inicia a busca de dados do desafio.
     *
     * @param obj jsonElement com os dados da srm.
     *
     * @return retorna um desafio seja ele ja cadastrado ou novo.
     */
    public static Maratona buscaPorIdTopCoder(JsonElement obj) {
        Long id = obj.getAsJsonObject().get("id").getAsLong();
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Maratona obj where obj.idMaratona = :id ");
        q.setParameter("id", id);
        q.setMaxResults(1);
        List<Maratona> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return buscarMaratona(obj);
        }
        return resultList.get(0);
    }

    private static Maratona buscarMaratona(JsonElement obj) {
        Maratona maratona = new Maratona();
        maratona.setIdMaratona(obj.getAsJsonObject().get("id").getAsLong());
        maratona.setNome(obj.getAsJsonObject().get("name").getAsString());
        maratona.setStatus(obj.getAsJsonObject().get("status").getAsString());

        maratona.setDataInicial(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("startDate").getAsString()));
        maratona.setDataFinal(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("endDate").getAsString()));
//        maratona.setNumeroRegistrados(obj.getAsJsonObject().get("numRegistrants").getAsJsonArray().get(0).getAsInt());
        maratona.setNumeroRegistrados(obj.getAsJsonObject().get("userIds").getAsJsonArray().size());

        for (JsonElement json : obj.getAsJsonObject().get("rounds").getAsJsonArray()) {
            maratona.getRounds().add(buscaRound(maratona, json));
        }
        GenericCrudBean.getInstance().inserir(maratona);
        return maratona;
    }

    private static MaratonaRound buscaRound(Maratona maratona, JsonElement json) {
        MaratonaRound round = new MaratonaRound(maratona);
        try {
            round.setInicioRegisto(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("registrationStartAt").getAsString()));
            round.setFimRegistro(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("registrationEndAt").getAsString()));
            round.setInicioCodificacao(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("codingStartAt").getAsString()));
            round.setFimCodificacao(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("codingEndAt").getAsString()));
            round.setIdRound(json.getAsJsonObject().get("id").getAsLong());

        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
        try {
            if (!json.getAsJsonObject().get("forumId").isJsonNull()) {
                round.setForumId(json.getAsJsonObject().get("forumId").getAsLong());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return round;
    }

    private static List<DetalheMembroMaratona> gerarDetalhes(JsonElement obj, MembroMaratona membroMaratona) {
        List<DetalheMembroMaratona> retornar = new ArrayList<>();
        for (JsonElement json : obj.getAsJsonObject().get("rounds").getAsJsonArray()) {
            DetalheMembroMaratona detalhes = new DetalheMembroMaratona(membroMaratona);
            try {
                
                if (json.getAsJsonObject().get("userMMDetails") == null ||
                        !json.getAsJsonObject().get("userMMDetails").isJsonArray()) {
                    continue;
                }
                if (!json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("newRating").isJsonNull()) {
                    detalhes.setNovaPontuacao(json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("newRating").getAsDouble());
                }
                if (!json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("oldRating").isJsonNull()) {
                    detalhes.setAntigaPontuacao(json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("oldRating").getAsDouble());
                }
                if (!json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("ovarallRank").isJsonNull()) {
                    detalhes.setClassificacaoGeral(json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("ovarallRank").getAsInt());
                }

                detalhes.setColocacao(json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("rated").getAsInt());
                if (!json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("pointTotal").isJsonNull()) {
                    detalhes.setTotalPontos(json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("pointTotal").getAsDouble());
                }
                detalhes.setNumSubmissoes(json.getAsJsonObject().get("userMMDetails").getAsJsonObject().get("numSubmissions").getAsInt());

                if (!json.getAsJsonObject().get("forumId").isJsonNull()) {
                    detalhes.setMaratonaRound(MaratonaRoundService.buscaPorIdTopCoder(json.getAsJsonObject().get("id").getAsLong()));
                }

                retornar.add(detalhes);
            } catch (NullPointerException e) {
//                e.printStackTrace();
            }

        }
        return retornar;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.Track;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class TrackService {

    public static Track busca(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Track obj where obj.nome like :nome");
        q.setParameter("nome", "%" + nome + "%");
        List<Track> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            Track t = new Track(nome.trim());
            GenericCrudBean.getInstance().inserir(t);
            return t;
        }
        return resultList.get(0);
    }

}

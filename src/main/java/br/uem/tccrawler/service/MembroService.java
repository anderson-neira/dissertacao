/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.DataUtil;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.Membro;
import br.uem.tccrawler.model.UltimaSubmissao;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class MembroService {

    private static final String REMOVER = "REMOVER";
    private static final String RESULT = "result";
    private static final String CONTENT = "content";
    private static final String STATSV2 = "https://api.topcoder.com/v2/users/" + REMOVER + "/stats/";
    private static final String STATSV3 = "https://api.topcoder.com/v3/members/" + REMOVER + "/";
    private static final String STATSV3AVANCADO = "https://api.topcoder.com/v3/members/" + REMOVER + "/stats/";

    /**
     * Metodo responsável chamar os metodos que vão na API do Top Coder e buscar
     * os dados do membro.
     *
     * @param membro
     * @return membro
     */
    public static Membro buscaMembroNaAPI(Membro membro) {

        membro = buscaDadosBasicosMembro(membro);
        membro = buscaDadosAvancadosMembro(membro);
        membro = HabilidadeSevice.buscarHabilidades(membro);
        membro = DesafioDesenvolvimentoService.buscaOsDesafiosDeDesenvolvimento(membro);
        membro = DesafioDesignService.buscaOsDesafiosDeDesign(membro);
        membro = SrmService.buscaSrms(membro);
        membro = MaratonaService.buscaMaratona(membro);
        membro = ConquistaService.buscarConquistas(membro);

        return membro;
    }

    /**
     * O objetivo deste método é buscar os dados básicos de um membro. Obs: url
     * utilizada https://api.topcoder.com/v2/users/andersonneira/stats/ E
     * https://api.topcoder.com/v3/members/monicamuranyi/
     *
     * @param entidade é passado como parametro o membro que deseja-se buscar os
     * dados básicos, é indispensável que nao parametro o nome seja enviado
     *
     * @return o membro com o dados;
     */
    private static Membro buscaDadosBasicosMembro(Membro entidade) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(STATSV2.replace(REMOVER, entidade.getNome()));
            entidade.setPais(resp.get("country").getAsString());
            entidade.setMembroDesde(DataUtil.converterJavaScriptDateToJavaDate(resp.get("memberSince").getAsString()));
            entidade.setCopiloto(Boolean.valueOf(resp.get("copilot").getAsString()));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(STATSV3.replace(REMOVER, entidade.getNome()));
            entidade.setStatus(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("status").getAsString());
            entidade.setUltimoUpdateConta(DataUtil.convertUTCStringToJavaDate(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("updatedAt").getAsString()));

            if (resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("tracks").isJsonArray()) {
                for (JsonElement obj : resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("tracks").getAsJsonArray()) {
                    entidade.getTracks().add(TrackService.busca(obj.getAsString()));
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return entidade;
    }

    /**
     * O objetivo deste método é buscar os dados avancados de um membro. Obs:
     * url utilizada https://api.topcoder.com/v3/members/andersonneira/stats/
     *
     * @param entidade
     * @return o membro com o dados;
     */
    private static Membro buscaDadosAvancadosMembro(Membro entidade) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(STATSV3AVANCADO.replace(REMOVER, entidade.getNome()));
            entidade.setUserId(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("userId").getAsLong());
            entidade.setQtdeDesafio(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("challenges").getAsInt());
            entidade.setQtdeVitoria(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("wins").getAsInt());
            if (!resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("DATA_SCIENCE").getAsJsonObject().get("SRM").getAsJsonObject().get("rank").isJsonNull()) {
                entidade.setLinguagemPadraoSrm(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("DATA_SCIENCE").getAsJsonObject().get("SRM").getAsJsonObject().get("rank").getAsJsonObject().get("defaultLanguage").getAsString());
            }

            entidade = completaStatisticas(entidade, resp);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entidade;
    }

    /**
     * Verifica se o usuario tem estaticas de DEVELOP - DATA_SCIENCE ou de
     * DESIGN
     *
     * @param entidade
     * @param resp
     * @return membro
     */
    private static Membro completaStatisticas(Membro entidade, JsonObject resp) {

        try {
            UltimaSubmissao dev = new UltimaSubmissao("Develop", entidade);
            dev.setUltimaSubmissao(DataUtil.converterJavaScriptDateToJavaDate(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("DEVELOP").getAsJsonObject().get("mostRecentSubmission").getAsString()));
            entidade.getUltimasSubmissoes().add(dev);
        } catch (NullPointerException ex) {
        }
        try {
            UltimaSubmissao des = new UltimaSubmissao("Design", entidade);
            des.setUltimaSubmissao(DataUtil.converterJavaScriptDateToJavaDate(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("DESIGN").getAsJsonObject().get("mostRecentSubmission").getAsString()));
            entidade.getUltimasSubmissoes().add(des);
        } catch (NullPointerException ex) {
        }
        try {
            UltimaSubmissao dts = new UltimaSubmissao("Data_Science", entidade);
            dts.setUltimaSubmissao(DataUtil.converterJavaScriptDateToJavaDate(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonObject().get("DATA_SCIENCE").getAsJsonObject().get("mostRecentSubmission").getAsString()));
            entidade.getUltimasSubmissoes().add(dts);
        } catch (NullPointerException ex) {
        }
        return entidade;
    }

    public static List<Membro> listaTeste() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
//        Query q = em.createQuery(" from Membro obj where obj.id = :id");
        Query q = em.createQuery(" from Membro obj ");
//        q.setParameter("id", UUID.fromString("1bbb9bf0-99d2-4798-9be8-f3a8e68a680d"));         
        return q.getResultList();
    }

    public static Membro buscaPorNome(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery(" from Membro obj where obj.nome = :nome ");
        q.setParameter("nome", nome);
        List<Membro> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return buscaMembroNaAPI(new Membro(nome));
        }
        return resultList.get(0);
    }
    public static String buscaIdPorNome(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createNativeQuery("select id from membro obj where obj.nome = :nome ");
        q.setParameter("nome", nome);
        List<String> resultList = q.getResultList();
        return resultList.get(0);
    }

    public static Membro buscaPorNomeSemCriarNovo(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery(" from Membro obj where obj.nome = :nome ");
        q.setParameter("nome", nome);
        List<Membro> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        return resultList.get(0);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.SrmRound;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class SrmRoundService {

    public static SrmRound buscaPorIdTopCoder(Long id) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from SrmRound obj where obj.idRound = :id ");
        q.setParameter("id", id);
        q.setMaxResults(1);
        List<SrmRound> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        return resultList.get(0);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author anderson
 */
public class HttpUtil {

    public static JsonObject enviarHttpGet(String url) throws IOException {

        url = validarUrl(url);
        //cria a requisição
        HttpGet req = new HttpGet(url);
        //envia a requisição
        int timeOut = 30;
        try {

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(timeOut * 1000)
                    .setConnectionRequestTimeout(timeOut * 1000)
                    .setSocketTimeout(timeOut * 1000).build();

            HttpResponse response = HttpClientBuilder
                    .create().setDefaultRequestConfig(config)
                    .build().execute(req);
            return entityToGsonObj(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
//        try {
//            RequestConfig config = RequestConfig.custom()
//                    .setConnectTimeout(timeOut * 1000)
//                    .setConnectionRequestTimeout(timeOut * 1000)
//                    .setSocketTimeout(timeOut * 1000).build();
//
//            HttpResponse response = HttpClientBuilder
//                    .create().setDefaultRequestConfig(config)
//                    .build().execute(req);
//            return entityToGsonObj(response.getEntity());
//        } catch (Exception e) {
//
//        }

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeOut * 1000)
                .setConnectionRequestTimeout(timeOut * 1000)
                .setSocketTimeout(timeOut * 1000).build();

        HttpResponse response = HttpClientBuilder
                .create().setDefaultRequestConfig(config)
                .build().execute(req);
        // transforma a resposta em JSON (para poder ficar mais facil pegar os dados)
        return entityToGsonObj(response.getEntity());
    }

    public static JsonArray enviarHttpGetReturnArray(String url) throws IOException {

        url = validarUrl(url);
        //cria a requisição
        HttpGet req = new HttpGet(url);
        //envia a requisição
        int timeOut = 30;
        try {

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(timeOut * 1000)
                    .setConnectionRequestTimeout(timeOut * 1000)
                    .setSocketTimeout(timeOut * 1000).build();

            HttpResponse response = HttpClientBuilder
                    .create().setDefaultRequestConfig(config)
                    .build().execute(req);
            return entityToGsonArray(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
//        try {
//            RequestConfig config = RequestConfig.custom()
//                    .setConnectTimeout(timeOut * 1000)
//                    .setConnectionRequestTimeout(timeOut * 1000)
//                    .setSocketTimeout(timeOut * 1000).build();
//
//            HttpResponse response = HttpClientBuilder
//                    .create().setDefaultRequestConfig(config)
//                    .build().execute(req);
//            return entityToGsonObj(response.getEntity());
//        } catch (Exception e) {
//
//        }

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeOut * 1000)
                .setConnectionRequestTimeout(timeOut * 1000)
                .setSocketTimeout(timeOut * 1000).build();

        HttpResponse response = HttpClientBuilder
                .create().setDefaultRequestConfig(config)
                .build().execute(req);
        // transforma a resposta em JSON (para poder ficar mais facil pegar os dados)
        return entityToGsonArray(response.getEntity());
    }

    public static String enviarHttpGetRetornandoString(String url) throws IOException {
        url = validarUrl(url);
        //cria a requisição
        HttpGet req = new HttpGet(url);
        //envia a requisição
        HttpResponse response = HttpClientBuilder.create().build().execute(req);
        // transforma a resposta em JSON (para poder ficar mais facil pegar os dados)
        return entityToString(response.getEntity());
    }

    private static JsonObject entityToGsonObj(HttpEntity data) {
        StringBuilder str = new StringBuilder();
        try {
            InputStream is = data.getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            String line = null;
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    str.append(line + "\n");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }

        } catch (IOException e) {
        }

        return new JsonParser().parse(str.toString()).getAsJsonObject();
    }
    private static JsonArray entityToGsonArray(HttpEntity data) {
        StringBuilder str = new StringBuilder();
        try {
            InputStream is = data.getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            String line = null;
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    str.append(line + "\n");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }

        } catch (IOException e) {
        }

        return new JsonParser().parse(str.toString()).getAsJsonArray();
    }

    private static String entityToString(HttpEntity data) {
        StringBuilder str = new StringBuilder();
        try {
            InputStream is = data.getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            String line = null;
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    str.append(line + "\n");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }

        } catch (IOException e) {
        }
        return str.toString();
    }

    private static String validarUrl(String url) {
        if (url.contains("{")) {
            url = url.replace("{", "%7B");
        }

        if (url.contains("}")) {
            url = url.replace("}", "%7D");
        }

        if (url.contains("|")) {
            url = url.replace("|", "%7C");
        }
        return url;
    }
}

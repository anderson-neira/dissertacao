/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.DesafioDesenvolvimento;
import br.uem.tccrawler.model.DesafioDesign;
import br.uem.tccrawler.model.Plataforma;
import br.uem.tccrawler.model.Tecnologia;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class TecnologiaService {

    public static Tecnologia busca(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Tecnologia obj where lower(obj.nome) like lower(:nome)");
        q.setParameter("nome", nome);
        List<Tecnologia> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            Tecnologia t = new Tecnologia(nome.trim());
           if (em.getTransaction().isActive()) {
                GenericCrudBean.getInstance().inserir(t);
            }else{
                em.getTransaction().begin();
                GenericCrudBean.getInstance().inserir(t);
                em.getTransaction().commit();
            }
            return t;
        }
        return resultList.get(0);
    }

    public static List<Tecnologia> buscaTodosOrdenado() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Tecnologia order by nome");
        return q.getResultList();
    }

    public static List<Tecnologia> buscaTodosAgrupradoOrdenado() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery(" from Tecnologia  group by opcao03 order by opcao03");
        return q.getResultList();
    }

    public static void removerTecnologiasDuplicadas() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        List<Tecnologia> resultList = em.createQuery("from Tecnologia obj group by obj.nome").getResultList();
        System.out.println(resultList.size());
        
        List<String> resultList2 = em.createNativeQuery("select id from desafiodesign obj where obj.criadoEm > 01/11/2017").getResultList();
        System.out.println(resultList2.size());
      
        for (String dd : resultList2) {
            DesafioDesign desafio = (DesafioDesign) GenericCrudBean.getInstance().recuperar(DesafioDesign.class, dd);
            List<Tecnologia> remover = new ArrayList<>(desafio.getTecnologias());
            desafio.getTecnologias().removeAll(remover);
            for (Tecnologia t : remover) {
                for (Tecnologia t2 : resultList) {
                    if (t2.getNome().toLowerCase().trim().equals(t.getNome().toLowerCase().trim())) {
                        desafio.getTecnologias().add(t2);
                    }
                }
            }
            GenericCrudBean.getInstance().alterar(desafio);
        }
    }

    public static void removerPlataformasDuplicadas() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        List<Plataforma> resultList = em.createQuery("from Plataforma obj group by obj.nome").getResultList();
        List<DesafioDesenvolvimento> resultList2 = em.createQuery("from DesafioDesenvolvimento obj").getResultList();

        for (DesafioDesenvolvimento desafio : resultList2) {
            List<Plataforma> remover = new ArrayList<>(desafio.getPlataformas());
            desafio.getPlataformas().removeAll(remover);
            for (Plataforma t : remover) {
                for (Plataforma t2 : resultList) {
                    if (t2.getNome().toLowerCase().trim().equals(t.getNome().toLowerCase().trim())) {
                        desafio.getPlataformas().add(t2);
                    }
                }
            }
            GenericCrudBean.getInstance().alterar(desafio);
        }
    }

//    public static void removerTecnologiasDuplicadas() {
//        EntityManager em = MuniFilter.getEntityManagerCorrente();
//        List<Tecnologia> resultList = em.createQuery("from Tecnologia obj group by obj.nome").getResultList();
//        List<DesafioDesign> resultList2 = em.createQuery("from DesafioDesign obj").getResultList();
//
//        for (DesafioDesign desafio : resultList2) {
//            List<Tecnologia> remover = new ArrayList<>(desafio.getTecnologias());
//            desafio.getTecnologias().removeAll(remover);
//            for (Tecnologia t : remover) {
//                for (Tecnologia t2 : resultList) {
//                    if (t2.getNome().toLowerCase().trim().equals(t.getNome().toLowerCase().trim())) {
//                        desafio.getTecnologias().add(t2);
//                    }
//                }
//            }
//            GenericCrudBean.getInstance().alterar(desafio);
//        }
//    }
//    
//    public static void removerPlataformasDuplicadas() {
//        EntityManager em = MuniFilter.getEntityManagerCorrente();
//        List<Plataforma> resultList = em.createQuery("from Plataforma obj group by obj.nome").getResultList();
//        List<DesafioDesign> resultList2 = em.createQuery("from DesafioDesign obj").getResultList();
//
//        for (DesafioDesign desafio : resultList2) {
//            List<Plataforma> remover = new ArrayList<>(desafio.getPlataformas());
//            desafio.getPlataformas().removeAll(remover);
//            for (Plataforma t : remover) {
//                for (Plataforma t2 : resultList) {
//                    if (t2.getNome().toLowerCase().trim().equals(t.getNome().toLowerCase().trim())) {
//                        desafio.getPlataformas().add(t2);
//                    }
//                }
//            }
//            GenericCrudBean.getInstance().alterar(desafio);
//        }
//    }
}

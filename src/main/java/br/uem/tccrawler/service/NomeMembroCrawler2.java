/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.NomeMembro;
import br.uem.tccrawler.model.PaisAux;
import java.io.IOException;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author anderson
 */
public class NomeMembroCrawler2 {

    private static final int BASE = 15;
    private static final String INICIO = "#INICIO#";
    private static final String FIM = "#FIM#";
    private static final String PAIS = "#PAIS#";
    private static final String URLBASE = "http://www.topcoder.com/";
    private static final String URLPARAM = "tc?module=AdvancedSearch&sr=" + INICIO + "&er=" + FIM + "&sc=&sd=&ha=&st=&cc=" + PAIS + "&sn=&minra=&maxra=&nrn=&mrn=&mdslc=&minconra=&maxconra=&minspera=&maxspera=&minarcra=&maxarcra=&mindsra=&maxdsra=&mindvra=&maxdvra=&minassra=&maxassra=&mintesra=&maxtesra=&mintscra=&maxtscra=&minuira=&maxuira=&minriara=&maxriara=&minccra=&maxccra=&minrepra=&maxrepra=&hsminra=&hsmaxra=&hsnrn=&hsmrn=&hsmdslc=&marminra=&marmaxra=&marnrn=&marmrn=&marmdslc=#jump";

    public static void buscaNomeMembro() throws IOException {

        int inicio = 1;
        int fim = 15;
        int sair = 1000000;
        for (int x = 0; inicio < sair; x++) {
            String url = URLBASE + URLPARAM.replace(INICIO, "" + inicio).replace(FIM, "" + fim).replace(PAIS, "156");
            Document doc = null;

            while (doc == null) {
                try {
                    System.out.println(doc);
                    doc = Jsoup.connect(url).get();
                    
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (sair == 1000000) {
                sair = Integer.valueOf(doc.select(".pagingBox").get(0).getElementsByTag("strong").last().text());
            }
            MuniFilter.getEntityManagerCorrente().getTransaction().begin();
            for (Element json : doc.select(".value")) {
                if (!json.getElementsByTag("a").isEmpty()) {
                    GenericCrudBean.getInstance().inserir(new NomeMembro(json.getElementsByTag("a").get(0).text()));
                }
            }
            MuniFilter.getEntityManagerCorrente().getTransaction().commit();
            inicio = inicio + BASE;
            fim = fim + BASE;
        }

    }

    public static List<PaisAux> buscaPaisAux() {
        return MuniFilter.getEntityManagerCorrente().createQuery("from PaisAux obj").getResultList();
    }
}

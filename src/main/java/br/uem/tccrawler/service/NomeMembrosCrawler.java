/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.NomeMembro;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author anderson
 */
public class NomeMembrosCrawler {

    public static final String INDEX = "INDEX";
    public static final String URL = "http://api.topcoder.com/v2/users/tops/data?pageIndex=" + INDEX + "&pageSize=40";

    public static void buscaNomeMembro() throws InterruptedException {
        int x;
        
        for (x = 1525; x < 10000; x++) { // local           
            long inicio = System.currentTimeMillis();
            System.out.println("começou a pagina " + x);
            buscar(x);
            System.out.println("terminou a pagina " + x + " em : " + (System.currentTimeMillis() - inicio));
        }
        gravarMensagemArquivo("Terminou como iterador em " + x);
    }

    private static void buscar(int index) {
        try {
            JsonObject resp;
            resp = HttpUtil.enviarHttpGet(URL.replace(INDEX, "" + index));
            MuniFilter.getEntityManagerCorrente().getTransaction().begin();
            for (JsonElement json : resp.get("data").getAsJsonArray()) {
                GenericCrudBean.getInstance().inserir(new NomeMembro(json.getAsJsonObject().get("handle").getAsString()));
            }
            MuniFilter.getEntityManagerCorrente().getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Deu problema no iterador " + index + ". Erro: " + e.getMessage() + "\n");
            gravarMensagemArquivo("Deu problema no iterador " + index + ". Erro: " + e.getMessage() + "\n");
            e.printStackTrace();
        }

    }

    private static void gravarMensagemArquivo(String mensagem) {
        try {
            File file = new File("/home/anderson/logTcCrawler.txt");
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(mensagem);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

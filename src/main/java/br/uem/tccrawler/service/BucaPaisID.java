/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.uem.tccrawler.model.PaisAux;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author anderson
 */
public class BucaPaisID {

    public static void buscaPaginaPais() throws IOException {
        for (JsonElement obj : enviarHttpGet("http://api.topcoder.com/v2/data/countries")) {
            GenericCrudBean.getInstance().inserir(new PaisAux(obj.getAsJsonObject().get("countryCode").getAsInt()));
        }

    }

    public static JsonArray enviarHttpGet(String url) throws IOException {
        //cria a requisição
        HttpGet req = new HttpGet(url);
        //envia a requisição
        HttpResponse response = HttpClientBuilder.create().build().execute(req);
        // transforma a resposta em JSON (para poder ficar mais facil pegar os dados)
        return entityToGsonObj(response.getEntity());
    }

    private static JsonArray entityToGsonObj(HttpEntity data) {
        StringBuilder str = new StringBuilder();
        try {
            InputStream is = data.getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            String line = null;
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    str.append(line + "\n");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }

        } catch (IOException e) {
        }

        return new JsonParser().parse(str.toString()).getAsJsonArray();
    }

}

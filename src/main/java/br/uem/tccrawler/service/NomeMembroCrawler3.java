/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.DesafioDesenvolvimento;
import br.uem.tccrawler.model.DesafioDesign;
import br.uem.tccrawler.model.NomeMembro;
import com.google.gson.JsonArray;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 *
 * @author anderson
 */
public class NomeMembroCrawler3 {

    public static final String INDEX = "#INDEX#";
    public static final String URL = "http://api.topcoder.com/v2/challenges/registrants/" + INDEX ;

    public static void buscaNomeMembro() throws InterruptedException {
        for (DesafioDesign  d : DesafioDesignService.listaTodos()) {
            long inicio = System.currentTimeMillis();
            System.out.println("começou a pagina " + d.getIdDesafio());
            buscar(d.getIdDesafio());
            System.out.println("terminou a pagina " + d.getIdDesafio() + " em : " + (System.currentTimeMillis() - inicio));
        }

    }

    private static void buscar(long index) {
        try {
            JsonArray resp = HttpUtil.enviarHttpGetReturnArray(URL.replace(INDEX, "" + index));
            MuniFilter.getEntityManagerCorrente().getTransaction().begin();
            for (JsonElement json : resp) {
                GenericCrudBean.getInstance().inserir(new NomeMembro(json.getAsJsonObject().get("handle").getAsString()));
            }
            MuniFilter.getEntityManagerCorrente().getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Deu problema no iterador " + index + ". Erro: " + e.getMessage() + "\n");

            e.printStackTrace();
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.DataUtil;
import br.uem.tccrawler.model.Conquista;
import br.uem.tccrawler.model.Membro;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.UUID;

/**
 *
 * @author anderson
 */
public class ConquistaService {

    private static final String REMOVER = "REMOVER";
    private static final String CONQUISTA = "https://api.topcoder.com/v2/users/" + REMOVER;

    public static Membro buscarConquistas(Membro entidade) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(CONQUISTA.replace(REMOVER, entidade.getNome()));
            for (JsonElement json : resp.get("Achievements").getAsJsonArray()) {
                Conquista conquista = new Conquista(entidade);
                conquista.setNome(json.getAsJsonObject().get("description").getAsString());
                conquista.setQuando(DataUtil.converterJavaScriptDateToJavaDate(json.getAsJsonObject().get("date").getAsString()));
                entidade.getConquistas().add(conquista);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entidade;
    }

    public static void buscarConquistas(String idMembro, String nome) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(CONQUISTA.replace(REMOVER, nome));
            for (JsonElement json : resp.get("Achievements").getAsJsonArray()) {

                String descricao = (json.getAsJsonObject().get("description").getAsString());

                String quando = DataUtil.format3(DataUtil.converterJavaScriptDateToJavaDate(json.getAsJsonObject().get("date").getAsString()));

                String idobj = UUID.randomUUID().toString();

                System.out.println("INSERT INTO tcc1.conquista (id, criadoEm, nome, quando, membro_id, novo) "
                        + "	VALUES ('"+idobj+"', '"+quando+"', '"+descricao+"', '"+quando+"', '"+idMembro+"', false); ");
            }
        } catch (IOException ex) {
            
        }

    }

}

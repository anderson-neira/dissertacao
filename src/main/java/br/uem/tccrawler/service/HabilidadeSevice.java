/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.Habilidade;
import br.uem.tccrawler.model.Membro;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class HabilidadeSevice {
    
    private static final String REMOVER = "REMOVER";
    private static final String SKILS = "https://api.topcoder.com/v3/members/" + REMOVER + "/skills/";

    /**
     * Busca todas as habilidade de um membro.
     *
     * @param entidade
     * @return membro
     *
     */
    public static Membro buscarHabilidades(Membro entidade) {
        try {
            String resposta = HttpUtil.enviarHttpGetRetornandoString(SKILS.replace(REMOVER, entidade.getNome()));
            
            String[] split1 = resposta.split("tagName\":");
            for (String s1 : split1) {
                String nome = (s1.split("\",")[0]);
                if (nome.startsWith("\"")) {
                    entidade.getHabilidades().add(busca(nome.replace("\"", "")));
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entidade;
    }
    
    public static Habilidade busca(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Habilidade obj where obj.nome like :nome");
        q.setParameter("nome", "%"+nome+"%");
        List<Habilidade> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            Habilidade t = new Habilidade(nome.trim());
            GenericCrudBean.getInstance().inserir(t);
            return t;
        }
        return resultList.get(0);
    }
}

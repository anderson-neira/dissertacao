/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.DataUtil;
import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.DetalheMembroSrm;
import br.uem.tccrawler.model.Membro;
import br.uem.tccrawler.model.MembroSrm;
import br.uem.tccrawler.model.Srm;
import br.uem.tccrawler.model.SrmRound;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class SrmService {

    private static final String REMOVER = "REMOVER";
    private static final String RESULT = "result";
    private static final String CONTENT = "content";
    private static final String LIMIT = "LIMIT";
    private static final String OFFSET = "OFFSET";
    private static final String METADATA = "metadata";

    private static final String SMRURL = "https://api.topcoder.com/v3/members/" + REMOVER + "/srms/?filter=track%3dDEVELOP&limit=" + LIMIT + "&offset=" + OFFSET + "&orderBy=endDate+desc";

    public static Membro buscaSrms(Membro entidade) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(SMRURL.replace(OFFSET, "0").replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
            entidade.setQtdeSrms(resp.get(RESULT).getAsJsonObject().get(METADATA).getAsJsonObject().get("totalCount").getAsInt());

            // tem dados para salvar
            if (entidade.getQtdeSrms() > 0) {
                // não precisa de mais de uma requisição
                if (entidade.getQtdeSrms() <= 50) {
                    entidade = geraSrm(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                } else {
                    // pega os desafios da primeira requisição
                    entidade = geraSrm(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);

                    // precisa de mais de uma reqsuicação                   
                    for (int x = 1; x < entidade.getQtdeSrms() / 50; x++) {
                        //pega a proxima pagina de desafios
                        JsonObject respMeio = HttpUtil.enviarHttpGet(SMRURL.replace(OFFSET, "" + 50 * x).replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
                        // para cada desafio cria um obj
                        entidade = geraSrm(respMeio.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                    }
                    // pega os ultimos dados       
                    String lim = entidade.getQtdeDesafiosDesenvolvimento() % 50 == 0 ? "50" : "" + entidade.getQtdeDesafiosDesenvolvimento() % 50;
                    JsonObject respUltima = HttpUtil.enviarHttpGet(SMRURL.replace(OFFSET, "" + (entidade.getQtdeSrms() / 50) * 50).replace(REMOVER, entidade.getNome()).replace(LIMIT, lim));
                    try {
                        entidade = geraSrm(respUltima.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entidade;
    }

    private static Membro geraSrm(JsonArray array, Membro entidade) {
        for (JsonElement obj : array) {
            Srm srm = buscaPorIdTopCoder(obj);
            MembroSrm membroSrm = new MembroSrm(entidade, srm);
            membroSrm.setDetalhesMembroSrm(gerarDetalhes(obj, membroSrm));

            entidade.getMembroSrms().add(membroSrm);
        }
        return entidade;
    }

    /**
     * Este método busca a srm pelo id do topCoder, caso não esteja cadastrado
     * ele inicia a busca de dados do desafio.
     *
     * @param obj jsonElement com os dados da srm.
     * @return retorna um desafio seja ele ja cadastrado ou novo.
     */
    public static Srm buscaPorIdTopCoder(JsonElement obj) {
        Long id = obj.getAsJsonObject().get("id").getAsLong();
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Srm obj where obj.srmId = :id ");
        q.setParameter("id", id);
        q.setMaxResults(1);
        List<Srm> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return buscarSrm(obj);
        }
        return resultList.get(0);
    }

    private static Srm buscarSrm(JsonElement obj) {
        Srm srm = new Srm();

        srm.setSrmId(obj.getAsJsonObject().get("id").getAsLong());
        srm.setNome(obj.getAsJsonObject().get("name").getAsString());
        srm.setStatus(obj.getAsJsonObject().get("status").getAsString());
        srm.setDataInicial(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("startDate").getAsString()));
        srm.setDataFinal(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("endDate").getAsString()));
//        srm.setNumeroRegistrados(obj.getAsJsonObject().get("numRegistrants").getAsJsonArray().get(0).getAsInt());
        for (JsonElement json : obj.getAsJsonObject().get("rounds").getAsJsonArray()) {
            SrmRound round = new SrmRound(srm);
            if (!json.getAsJsonObject().get("registrationStartAt").isJsonNull()) {
                try {
                    round.setInicioRegisto(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("registrationStartAt").getAsString()));
                    round.setFimRegistro(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("registrationEndAt").getAsString()));
                    round.setInicioCodificacao(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("codingStartAt").getAsString()));
                    round.setFimCodificacao(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("codingEndAt").getAsString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                round.setIdRound(json.getAsJsonObject().get("id").getAsLong());
            }

            try {
                if (!json.getAsJsonObject().get("forumId").isJsonNull()) {
                    round.setForumId(json.getAsJsonObject().get("forumId").getAsLong());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            srm.getRounds().add(round);
        }
        GenericCrudBean.getInstance().inserir(srm);
        return srm;
    }

    private static List<DetalheMembroSrm> gerarDetalhes(JsonElement obj, MembroSrm membroSrm) {
        List<DetalheMembroSrm> retornar = new ArrayList<>();

        for (JsonElement json : obj.getAsJsonObject().get("rounds").getAsJsonArray()) {
            DetalheMembroSrm detalhes = new DetalheMembroSrm(membroSrm);
            try {
                detalhes.setNovaClassificacao(json.getAsJsonObject().get("userSRMDetails").getAsJsonObject().get("newRating").getAsDouble());
                detalhes.setAntigaClassificacao(json.getAsJsonObject().get("userSRMDetails").getAsJsonObject().get("oldRating").getAsDouble());
                detalhes.setColocacao(json.getAsJsonObject().get("userSRMDetails").getAsJsonObject().get("rated").getAsInt());
                detalhes.setTotalPontos(json.getAsJsonObject().get("userSRMDetails").getAsJsonObject().get("finalPoints").getAsDouble());
                if (!json.getAsJsonObject().get("userSRMDetails").getAsJsonObject().get("divisionPlacement").isJsonNull()) {
                    detalhes.setColocacaoNaDivisao(json.getAsJsonObject().get("userSRMDetails").getAsJsonObject().get("divisionPlacement").getAsInt());
                }
                if (!json.getAsJsonObject().get("forumId").isJsonNull()) {
                    detalhes.setSrmRound(SrmRoundService.buscaPorIdTopCoder(json.getAsJsonObject().get("id").getAsLong()));
                }

                retornar.add(detalhes);
            } catch (NullPointerException e) {
//                e.printStackTrace();
            }
        }

        return retornar;
    }
}

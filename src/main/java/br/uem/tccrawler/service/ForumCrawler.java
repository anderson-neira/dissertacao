/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.DataUtil;
import br.com.munif.util.beans.GenericCrudBean;
import br.uem.tccrawler.model.Forum;
import br.uem.tccrawler.model.Membro;
import br.uem.tccrawler.model.Postagem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author anderson
 */
public class ForumCrawler {

    private static final String BASE = "https://apps.topcoder.com/forums/";
    private static final String ID = "#ID#";
    private static final String INICIAL = "?module=Thread&threadID=" + ID + "&start=0";

    public static void buscaPaginaForum(String threadID) throws IOException {
        try {
            Document doc = Jsoup.connect(BASE + INICIAL.replace(ID, threadID)).get();
            //criar post
            Forum forum = gerarForum(doc.body().select(".breadcrumbs"), threadID);

            // tabela de posts
            forum.setPostagem(gerarPost(doc.body().select(".rtTablePost"), forum));
            // buscar as proximas paginas

            List<String> paginas = procurarProximasPaginas(doc);
            if (!paginas.isEmpty()) {
                for (int x = 0; x < paginas.size() - 1; x++) {
                    Document docx = Jsoup.connect(BASE + INICIAL.replace(ID, threadID)).get();
                    forum.getPostagem().addAll(gerarPost(docx.body().select(".rtTablePost"), forum));
                }
            }
            GenericCrudBean.getInstance().inserir(forum);
        } catch (Exception e) {
        }
    }

    private static Forum gerarForum(Elements breadcrumbs, String id) {
        Forum retornar = new Forum(id);
        Element breadcrumb = breadcrumbs.get(0);

        Elements cats = breadcrumb.getElementsByTag("a");
        retornar.setCategoria(cats.get(1).text());
        retornar.setSubCategoria(cats.get(2).text());

        retornar.setNome(getNomeForum(breadcrumb.toString()));
        return retornar;
    }

    private static List<String> procurarProximasPaginas(Document doc) {
        try {
            List<String> linksProximasPagimas = new ArrayList<>();
            doc.body().select(".rtbc").first().getElementsByTag("a").forEach(l -> linksProximasPagimas.add(l.attr("href")));
            return linksProximasPagimas;
        } catch (NullPointerException ex) {

        }
        return new ArrayList<>();
    }

//    https://apps.topcoder.com/forums/?module=Thread&threadID=854948&start=0
    private static List<Postagem> gerarPost(Elements tables, Forum forum) {
        List<Postagem> postagens = new ArrayList<>();

        int ordem = 0;
        for (Element p : tables) {
            Postagem postagem = new Postagem(forum, ordem);

            Elements linksHeader = p.select(".rtHeader").get(0).getElementsByTag("a");
            if (linksHeader.get(0).text().contains("edit")) {
                postagem.setQuando(DataUtil.converterDatePostagem(linksHeader.get(1).text()));
            } else {
                postagem.setQuando(DataUtil.converterDatePostagem(linksHeader.get(0).text()));
            }

            Elements linksMembro = p.select(".rtPosterCell").get(0).getElementsByTag("a");
//            Membro membro = MembroService.buscaPorNome(linksMembro.get(0).text());
            Membro membro = MembroService.buscaPorNomeSemCriarNovo(linksMembro.get(0).text());
            if (membro == null) {
                continue;
            }

            postagem.setMembro(membro);
            if (forum.getAutor() == null) {
                forum.setAutor(membro);
            }

            postagem.setDescricao(p.select(".rtTextCell").get(0).text());

            System.out.println(postagem);
            postagens.add(postagem);
            ordem++;
        }

        return postagens;
    }

    private static String getNomeForum(String s) {
        String[] split = s.split(">");
        String[] split1 = split[split.length - 2].split("<");
        return (split1[0]);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.DataUtil;
import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.DesafioDesenvolvimento;
import br.uem.tccrawler.model.Membro;
import br.uem.tccrawler.model.MembroDesafioDesenvolvimento;
import br.uem.tccrawler.model.Regra;
import br.uem.tccrawler.model.SubmissaoDesenvolvimento;
import br.uem.tccrawler.model.SubmissaoDesenvolvimentoVencedora;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class DesafioDesenvolvimentoService {

    private static final String REMOVER = "REMOVER";
    private static final String RESULT = "result";
    private static final String CONTENT = "content";
    private static final String LIMIT = "LIMIT";
    private static final String OFFSET = "OFFSET";
    private static final String DEVELOPS = "https://api.topcoder.com/v3/members/" + REMOVER + "/challenges/?filter=track%3dDEVELOP&limit=" + LIMIT + "&offset=" + OFFSET + "&orderBy=submissionEndDate";
    private static final String METADATA = "metadata";

    /**
     * O objetivo deste método é buscar os dados avancados de um membro. Obs:
     * url utilizada https://api.topcoder.com/v3/members/andersonneira/stats/
     *
     * @param entidade
     * @return o membro com o dados;
     */
    public static Membro buscaOsDesafiosDeDesenvolvimento(Membro entidade) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(DEVELOPS.replace(OFFSET, "0").replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
            entidade.setQtdeDesafiosDesenvolvimento(resp.get(RESULT).getAsJsonObject().get(METADATA).getAsJsonObject().get("totalCount").getAsInt());

            // tem dados para salvar
            if (entidade.getQtdeDesafiosDesenvolvimento() > 0) {
                // não precisa de mais de uma requisição
                if (entidade.getQtdeDesafiosDesenvolvimento() <= 50) {
                    entidade = geraDesafioDesenvolvimento(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                } else {
                    // pega os desafios da primeira requisição
                    entidade = geraDesafioDesenvolvimento(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);

                    // precisa de mais de uma reqsuicação                   
                    for (int x = 1; x < entidade.getQtdeDesafiosDesenvolvimento() / 50; x++) {
                        //pega a proxima pagina de desafios
                        JsonObject respMeio = HttpUtil.enviarHttpGet(DEVELOPS.replace(OFFSET, "" + 50 * x).replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
                        // para cada desafio cria um obj
                        try {
                            entidade = geraDesafioDesenvolvimento(respMeio.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    String lim = entidade.getQtdeDesafiosDesenvolvimento() % 50 == 0 ? "50" : "" + entidade.getQtdeDesafiosDesenvolvimento() % 50;

                    // pega os ultimos dados                     
                    JsonObject respUltima = HttpUtil.enviarHttpGet(DEVELOPS.replace(OFFSET, "" + (entidade.getQtdeDesafiosDesenvolvimento() / 50) * 50).replace(REMOVER, entidade.getNome()).replace(LIMIT, lim));
                    entidade = geraDesafioDesenvolvimento(respUltima.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entidade;
    }

    private static Membro geraDesafioDesenvolvimento(JsonArray array, Membro entidade) {

        for (JsonElement obj : array) {
            // VERIFICA SE O DESAFIO JA ESTÁ CADASTRADO
            DesafioDesenvolvimento desa = buscaPorIdTopCoder(obj);
            // BUSCA AS SUBMISSOES
            MembroDesafioDesenvolvimento mdd = buscarSubmissoes(entidade, desa, obj);
            mdd.setSubmissaoDesenvolvimentoVencedora(buscarSubmissaoVencedora(obj, mdd));
            entidade.getMembroDesafiosDesenvolvimento().add(mdd);
        }

        return entidade;
    }

    /**
     * MÉTODO RESPONSÁVEL POR PEGAR OS DADOS DO DESAFIO E CRIAR (INSERIR) UM
     * NOVO DESAFIO COM AS NOVAS INFORMAÇÕES.
     *
     * @param obj json obj que contém os dados
     * @param desafio entidade desafio nova já com o id do desafio.
     */
    private static DesafioDesenvolvimento buscarDesafio(JsonElement obj, DesafioDesenvolvimento desafio) {
        desafio.setAtualizado(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("updatedAt").getAsString()));
        desafio.setCriado(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("createdAt").getAsString()));
        for (String tec : obj.getAsJsonObject().get("technologies").getAsString().split(",")) {
            desafio.getTecnologias().add(TecnologiaService.busca(tec));
        }
        desafio.setSubGrupo(obj.getAsJsonObject().get("subTrack").getAsString());
        desafio.setNome(obj.getAsJsonObject().get("name").getAsString());
        try {
            desafio.setForumId(obj.getAsJsonObject().get("forumId").getAsLong());
        } catch (Exception e) {
            e.printStackTrace();
        }

        desafio.setNumSubmissoes(obj.getAsJsonObject().get("numSubmissions").getAsInt());
        desafio.setNumRegistrados(obj.getAsJsonObject().get("numRegistrants").getAsInt());
        desafio.setInicioRegistro(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("registrationStartDate").getAsString()));
        desafio.setFimRegistro(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("registrationStartDate").getAsString()));
        desafio.setFinalSubmissao(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("submissionEndDate").getAsString()));
        for (String plat : obj.getAsJsonObject().get("platforms").getAsString().split(",")) {
            desafio.getPlataformas().add(PlataformaService.busca(plat));
        }
        desafio.setTotalRecompensa(obj.getAsJsonObject().get("totalPrize").getAsBigDecimal());
        GenericCrudBean.getInstance().inserir(desafio);
        return desafio;
    }

    /**
     * Buscas as submissoes de um desafio de um membro.
     *
     * @param entidade membro para fazer a ligação do hibernate
     * @param desafio desafio já completo com as informações
     * @param obj jsonElement com os dados das submissoẽs
     *
     */
    private static MembroDesafioDesenvolvimento buscarSubmissoes(Membro entidade, DesafioDesenvolvimento desafio, JsonElement obj) {
        MembroDesafioDesenvolvimento aux = new MembroDesafioDesenvolvimento(entidade, desafio);
        try {

            if (!obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("submissions").isJsonNull()) {

                for (JsonElement json : obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("submissions").getAsJsonArray()) {
                    SubmissaoDesenvolvimento sub = new SubmissaoDesenvolvimento(aux);
                    sub.setIdSubmissao(json.getAsJsonObject().get("id").getAsLong());
                    sub.setQuando(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("submittedAt").getAsString()));
                    sub.setStatus(json.getAsJsonObject().get("status").getAsString());
                    if (!json.getAsJsonObject().get("score").isJsonNull()) {
                        sub.setPontuacao(json.getAsJsonObject().get("score").getAsDouble());
                    }
                    if (!json.getAsJsonObject().get("placement").isJsonNull()) {
                        sub.setColocao(json.getAsJsonObject().get("placement").getAsInt());
                    }
                    aux.getSubmissoes().add(sub);
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return aux;
    }

    /**
     * Este método busca o desafio pelo id do topCoder, caso não esteja
     * cadastrado ele inicia a busca de dados do desafio.
     *
     * @param obj jsonElement com os dados do desafio.
     * @return retorna um desafio seja ele ja cadastrado ou novo.
     */
    public static DesafioDesenvolvimento buscaPorIdTopCoder(JsonElement obj) {
        Long id = obj.getAsJsonObject().get("id").getAsLong();
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from DesafioDesenvolvimento obj where obj.idDesafio = :id ");
        q.setParameter("id", id);
        q.setMaxResults(1);
        List<DesafioDesenvolvimento> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return buscarDesafio(obj, new DesafioDesenvolvimento(id));
        }
        return resultList.get(0);
    }

    public static List<DesafioDesenvolvimento> listaTodos() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from DesafioDesenvolvimento obj order by obj.idDesafio ");
        return q.getResultList();
    }

    public static List<DesafioDesenvolvimento> listaTodosApartirDe(long idDesafio) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from DesafioDesenvolvimento obj"
                + " where cast(obj.idDesafio as long) > :idDesafio"
                + " order by obj.idDesafio ");
        q.setParameter("idDesafio", idDesafio);
        return q.getResultList();
    }

    private static SubmissaoDesenvolvimentoVencedora buscarSubmissaoVencedora(JsonElement obj, MembroDesafioDesenvolvimento mdd) {
        try {
            if (!obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("winningPlacements").isJsonNull()) {
                SubmissaoDesenvolvimentoVencedora sub = new SubmissaoDesenvolvimentoVencedora(mdd);
                JsonElement json = obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("winningPlacements").getAsJsonArray().get(0);
                sub.setIdSubimissaoVencedora(json.getAsJsonObject().get("submissionId").getAsLong());
                sub.setRecompensaGanha(json.getAsJsonObject().get("amount").getAsBigDecimal());
                sub.setClassificacao(json.getAsJsonObject().get("placed").getAsInt());
                sub.setPontuacaoFinal(json.getAsJsonObject().get("finalScore").getAsDouble());
                return sub;
            }
        } catch (Exception e) {
        }
        return null;

    }

    public static void completaRegras(String entidade, String nome) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(DEVELOPS.replace(OFFSET, "0").replace(REMOVER, nome).replace(LIMIT, "10"));
            int desafios = resp.get(RESULT).getAsJsonObject().get(METADATA).getAsJsonObject().get("totalCount").getAsInt();

            // tem dados para salvar
            if (desafios > 0) {
                // não precisa de mais de uma requisição
                if (desafios <= 10) {
                    //   gerarRegras(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                    gerarSubmissoes(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                } else {
                    // pega os desafios da primeira requisição
                    //gerarRegras(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                    gerarSubmissoes(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);

                    // precisa de mais de uma reqsuicação                   
                    for (int x = 1; x < desafios / 10; x++) {
                        //pega a proxima pagina de desafios
                        JsonObject respMeio = HttpUtil.enviarHttpGet(DEVELOPS.replace(OFFSET, "" + 10 * x).replace(REMOVER, nome).replace(LIMIT, "10"));
                        // para cada desafio cria um obj
                        try {
                            //  gerarRegras(respMeio.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                            gerarSubmissoes(respMeio.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    String lim = desafios % 10 == 0 ? "10" : "" + desafios % 10;

                    // pega os ultimos dados                     
                    JsonObject respUltima = HttpUtil.enviarHttpGet(DEVELOPS.replace(OFFSET, "" + (desafios / 10) * 10).replace(REMOVER, nome).replace(LIMIT, lim));
//                    gerarRegras(respUltima.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                    gerarSubmissoes(respUltima.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                }
            }
        } catch (IOException ex) {
        }

    }

    public static String buscaMembroDesafioService(long idDesafio, String id) {

        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q1 = em.createNativeQuery("select mdd.id from membrodesafiodesenvolvimento  as mdd "
                + " inner join desafiodesenvolvimento as dd on dd.id = mdd.desafioDesenvolvimento_id "
                + " where mdd.membro_id = :membro "
                + " and dd.idDesafio= :desafio ");

        q1.setParameter("membro", id);
        q1.setParameter("desafio", idDesafio);

        String mddId = (String) q1.getResultList().get(0);

        return mddId;

    }

    public static Regra buscaRegras(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Regra obj where lower(obj.nome) like lower(:nome)");
        q.setParameter("nome", nome);
        List<Regra> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return new Regra(nome.trim());
        }
        return resultList.get(0);
    }

    private static void gerarRegras(JsonArray asJsonArray, String id) {
//        MuniFilter.getEntityManagerCorrente().getTransaction().begin();
//        EntityManager em = MuniFilter.getEntityManagerCorrente();
        for (JsonElement obj : asJsonArray) {
            try {

                String mddId = buscaMembroDesafioService(obj.getAsJsonObject().get("id").getAsLong(), id);

                JsonObject userDetails = obj.getAsJsonObject().get("userDetails").getAsJsonObject();

                Boolean tem = (userDetails.get("hasUserSubmittedForReview").getAsBoolean());
                if (tem) {
                    System.out.println("UPDATE membrodesafiodesenvolvimento SET temSubmisao = true where id = '" + mddId + "';");
                }
//
                for (JsonElement json : userDetails.get("roles").getAsJsonArray()) {
//
                    Regra regra = buscaRegras(json.getAsString());
//
//                    Query q = em.createNativeQuery("select * from tcc1.membrodesafiodesenvolvimento_regra "
//                            + "  where membrodesafiodesenvolvimento_id = :mdd "
//                            + " and regras_id =  :regra ");
//                    q.setParameter("mdd", mddId);
//                    q.setParameter("regra", regra.getId());
//
//                    List resultList = q.getResultList();
//                    if (resultList != null && !resultList.isEmpty() && resultList.get(0) != null) {
//                        System.out.println("ja tem- prox");
//                        continue;
//                    }
                    System.out.println("INSERT INTO tcc1.membrodesafiodesenvolvimento_regra (membrodesafiodesenvolvimento_id, regras_id)"
                            + "	VALUES ('" + mddId + "', '" + regra.getId() + "');");
//
                }
//
//                GenericCrudBean.getInstance().alterar(mdd);
            } catch (Exception e) {
//                e.printStackTrace();
//                System.out.println("nome:" + entidade.getNome() + " id desafio :" + obj.getAsJsonObject().get("id").getAsLong());
            }
        }
        try {
//            MuniFilter.getEntityManagerCorrente().getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void gerarSubmissoes(JsonArray asJsonArray, String id) {
        for (JsonElement obj : asJsonArray) {
            try {
                String mddId = buscaMembroDesafioService(obj.getAsJsonObject().get("id").getAsLong(), id);

                JsonObject userDetails = obj.getAsJsonObject().get("userDetails").getAsJsonObject();

                for (JsonElement json : userDetails.get("submissions").getAsJsonArray()) {

                    Long idSub = (json.getAsJsonObject().get("id").getAsLong());
                    Date quando = (DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("submittedAt").getAsString()));

                    String status = (json.getAsJsonObject().get("status").getAsString());
                    Double pontuacao = null;
                    Integer colocacao = null;
                    try {
                        pontuacao = (json.getAsJsonObject().get("score").getAsDouble());
                    } catch (Exception e) {
                    }

                    try {
                        colocacao = (json.getAsJsonObject().get("placement").getAsInt());
                    } catch (Exception e) {
                    }

                    String idobj = UUID.randomUUID().toString();
                    String dataSub = DataUtil.format3(quando);

                    System.out.println("INSERT INTO submissaodesenvolvimento (id, `criadoEm`, colocao, `idSubmissao`, pontuacao, quando, status, `membroDesafioDesenvolvimento_id`, novo) "
                            + "    VALUES ('" + idobj + "', '2017-12-08 00:00:00.0', " + colocacao + ", " + idSub + ", " + pontuacao + ", '" + dataSub + "', '" + status + "', '" + mddId + "', false);");

                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
        for (JsonElement obj : asJsonArray) {
            try {
                String mddId = buscaMembroDesafioService(obj.getAsJsonObject().get("id").getAsLong(), id);

                JsonObject userDetails = obj.getAsJsonObject().get("userDetails").getAsJsonObject();

                for (JsonElement json : userDetails.get("winningPlacements").getAsJsonArray()) {

                    Long idSub = (json.getAsJsonObject().get("submissionId").getAsLong());

                    BigDecimal recompensa = (json.getAsJsonObject().get("amount").getAsBigDecimal());
                    Double pontuacao = null;
                    Integer colocacao = null;
                    if (!json.getAsJsonObject().get("finalScore").isJsonNull()) {
                        pontuacao = (json.getAsJsonObject().get("finalScore").getAsDouble());
                    }
                    if (!json.getAsJsonObject().get("placed").isJsonNull()) {
                        colocacao = (json.getAsJsonObject().get("placed").getAsInt());
                    }

                    String idobj = UUID.randomUUID().toString();

                    System.out.println("INSERT INTO submissaodesenvolvimentovencedora (id, `criadoEm`, classificacao, `idSubimissaoVencedora`, `pontuacaoFinal`, `recompensaGanha`, `desafioDesenvolvimento_id`, novo) "
                            + "    VALUES ('" + idobj + "', '2017-12-08 00:00:00.0', " + colocacao + ", " + idSub + ", " + pontuacao + ", " + recompensa + ", '" + mddId + "', false);");

                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }

    }
}

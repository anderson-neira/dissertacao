/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.DataUtil;
import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.DesafioDesign;
import br.uem.tccrawler.model.Membro;
import br.uem.tccrawler.model.MembroDesafioDesign;
import br.uem.tccrawler.model.SubmissaoDesign;
import br.uem.tccrawler.model.SubmissaoDesignVencedora;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class DesafioDesignService {

    private static final String REMOVER = "REMOVER";
    private static final String RESULT = "result";
    private static final String CONTENT = "content";
    private static final String LIMIT = "LIMIT";
    private static final String OFFSET = "OFFSET";
    private static final String DESIGNURL = "https://api.topcoder.com/v3/members/" + REMOVER + "/challenges/?filter=track%3dDESIGN&limit=" + LIMIT + "&offset=" + OFFSET + "&orderBy=submissionEndDate+desc";

    private static final String METADATA = "metadata";

    public static Membro buscaOsDesafiosDeDesign(Membro entidade) {
        try {
            JsonObject resp = HttpUtil.enviarHttpGet(DESIGNURL.replace(OFFSET, "0").replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
            entidade.setQtdeDesafiosDesign(resp.get(RESULT).getAsJsonObject().get(METADATA).getAsJsonObject().get("totalCount").getAsInt());

            // tem dados para salvar
            if (entidade.getQtdeDesafiosDesign() > 0) {
                // não precisa de mais de uma requisição
                if (entidade.getQtdeDesafiosDesign() <= 50) {
                    entidade = geraDesafioDesign(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                } else {
                    // pega os desafios da primeira requisição
                    entidade = geraDesafioDesign(resp.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);

                    // precisa de mais de uma reqsuicação                   
                    for (int x = 1; x < entidade.getQtdeDesafiosDesign() / 50; x++) {
                        //pega a proxima pagina de desafios
                        JsonObject respMeio = HttpUtil.enviarHttpGet(DESIGNURL.replace(OFFSET, "" + 50 * x).replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
                        // para cada desafio cria um obj
                        
                        System.out.println(DESIGNURL.replace(OFFSET, "0").replace(REMOVER, entidade.getNome()).replace(LIMIT, "50"));
                        entidade = geraDesafioDesign(respMeio.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                    }
                    // pega os ultimos dados         
                    String lim = entidade.getQtdeDesafiosDesenvolvimento() % 50 == 0 ? "50" : "" + entidade.getQtdeDesafiosDesenvolvimento() % 50;
                    JsonObject respUltima = HttpUtil.enviarHttpGet(DESIGNURL.replace(OFFSET, "" + (entidade.getQtdeDesafiosDesign() / 50) * 50).replace(REMOVER, entidade.getNome()).replace(LIMIT, lim));
                    entidade = geraDesafioDesign(respUltima.get(RESULT).getAsJsonObject().get(CONTENT).getAsJsonArray(), entidade);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entidade;
    }

    private static Membro geraDesafioDesign(JsonArray array, Membro entidade) {

        for (JsonElement obj : array) {
            // VERIFICA SE O DESAFIO JA ESTÁ CADASTRADO
            DesafioDesign desa = buscaPorIdTopCoder(obj);
            // BUSCA AS SUBMISSOES
            MembroDesafioDesign mdd = buscaSubmissao(entidade, desa, obj);
            mdd.setSubmissaoDesignVencedora(buscaSubmissaoVencedora(obj, mdd));

            entidade.getMembroDesafiosDesign().add(mdd);
        }

        return entidade;
    }

    /**
     * MÉTODO RESPONSÁVEL POR PEGAR OS DADOS DO DESAFIO E CRIAR (INSERIR) UM
     * NOVO DESAFIO COM AS NOVAS INFORMAÇÕES.
     *
     * @param obj json obj que contém os dados
     * @param desafio entidade desafio nova já com o id do desafio.
     */
    private static DesafioDesign buscarDesafio(JsonElement obj, DesafioDesign desafio) {
        desafio.setAtualizado(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("updatedAt").getAsString()));
        desafio.setCriado(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("createdAt").getAsString()));

        for (String tec : obj.getAsJsonObject().get("technologies").getAsString().split(",")) {
            desafio.getTecnologias().add(TecnologiaService.busca(tec));
        }

        desafio.setSubGrupo(obj.getAsJsonObject().get("subTrack").getAsString());
        desafio.setNome(obj.getAsJsonObject().get("name").getAsString());
        desafio.setIdDesafio(obj.getAsJsonObject().get("id").getAsLong());

        try {
            desafio.setForumId(obj.getAsJsonObject().get("forumId").getAsLong());
        } catch (Exception e) {
            e.printStackTrace();
        }

        desafio.setNumSubmissoes(obj.getAsJsonObject().get("numSubmissions").getAsInt());
        desafio.setNumRegistrados(obj.getAsJsonObject().get("numRegistrants").getAsInt());

        desafio.setInicioRegistro(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("registrationStartDate").getAsString()));
        desafio.setFimRegistro(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("registrationStartDate").getAsString()));
        desafio.setFinalSubmissao(DataUtil.convertUTCStringToJavaDate(obj.getAsJsonObject().get("submissionEndDate").getAsString()));

        for (String plat : obj.getAsJsonObject().get("platforms").getAsString().split(",")) {
            desafio.getPlataformas().add(PlataformaService.busca(plat));
        }

        desafio.setTotalRecompensa(obj.getAsJsonObject().get("totalPrize").getAsBigDecimal());
        GenericCrudBean.getInstance().inserir(desafio);
        return desafio;

    }

    private static MembroDesafioDesign buscaSubmissao(Membro entidade, DesafioDesign desa, JsonElement obj) {
        MembroDesafioDesign aux = new MembroDesafioDesign(entidade, desa);
        try {

            if (!obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("submissions").isJsonNull()) {

                for (JsonElement json : obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("submissions").getAsJsonArray()) {
                    SubmissaoDesign sub = new SubmissaoDesign(aux);
                    sub.setIdSubmissao(json.getAsJsonObject().get("id").getAsLong());
                    sub.setQuando(DataUtil.convertUTCStringToJavaDate(json.getAsJsonObject().get("submittedAt").getAsString()));
                    sub.setStatus(json.getAsJsonObject().get("status").getAsString());
                    if (!json.getAsJsonObject().get("score").isJsonNull()) {
                        sub.setPontuacao(json.getAsJsonObject().get("score").getAsDouble());
                    }
                    if (!json.getAsJsonObject().get("placement").isJsonNull()) {
                        sub.setColocao(json.getAsJsonObject().get("placement").getAsInt());
                    }
                    aux.getSubmissoes().add(sub);
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return aux;
    }

    /**
     * Este método busca o desafio pelo id do topCoder, caso não esteja
     * cadastrado ele inicia a busca de dados do desafio.
     *
     * @param obj jsonElement com os dados do desafio.
     * @return retorna um desafio seja ele ja cadastrado ou novo.
     */
    public static DesafioDesign buscaPorIdTopCoder(JsonElement obj) {
        Long id = obj.getAsJsonObject().get("id").getAsLong();
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from DesafioDesign obj where obj.idDesafio = :id ");
        q.setParameter("id", id);
        q.setMaxResults(1);
        List<DesafioDesign> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return buscarDesafio(obj, new DesafioDesign(id));
        }
        return resultList.get(0);
    }

    public static List<DesafioDesign> listaTodos() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from DesafioDesign obj order by obj.idDesafio ");
        return q.getResultList();
    }

    private static SubmissaoDesignVencedora buscaSubmissaoVencedora(JsonElement obj, MembroDesafioDesign desafioDesign) {
        try {
            if (!obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("winningPlacements").isJsonNull()) {
                SubmissaoDesignVencedora sdv = new SubmissaoDesignVencedora(desafioDesign);
                JsonElement json = obj.getAsJsonObject().get("userDetails").getAsJsonObject().get("winningPlacements").getAsJsonArray().get(0);
                sdv.setIdSubimissaoVencedora(json.getAsJsonObject().get("submissionId").getAsLong());
                sdv.setRecompensaGanha(json.getAsJsonObject().get("amount").getAsBigDecimal());
                sdv.setClassificacao(json.getAsJsonObject().get("placed").getAsInt());
                sdv.setPontuacaoFinal(json.getAsJsonObject().get("finalScore").getAsDouble());
                return sdv;
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return null;
    }
}

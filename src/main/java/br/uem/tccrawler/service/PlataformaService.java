/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.Plataforma;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class PlataformaService {

    public static Plataforma busca(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Plataforma obj where lower(obj.nome) like lower(:nome)");
        q.setParameter("nome", nome);
        List<Plataforma> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            Plataforma t = new Plataforma(nome.trim());
            if (em.getTransaction().isActive()) {
                GenericCrudBean.getInstance().inserir(t);
            }else{
                em.getTransaction().begin();
                GenericCrudBean.getInstance().inserir(t);
                em.getTransaction().commit();
            }

            return t;
        }
        return resultList.get(0);
    }

}

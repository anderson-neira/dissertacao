/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.service;

import br.com.munif.util.web.MuniFilter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author anderson
 */
public class NomeMembroService {

    public static List<String> buscaNomes() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("select obj.nome from NomeMembro obj order by obj.nome ");
        return q.getResultList();
    }

//    public static void removerNomes(List<String> nomes) {
//        EntityManager em = MuniFilter.getEntityManagerCorrente();
//        em.createQuery("delete from NomeMembro obj where obj.nome in :lista ").executeUpdate();
//
//    }

}

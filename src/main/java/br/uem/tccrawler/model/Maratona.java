/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "maratona")
public class Maratona extends EntidadePadrao {

    private Long idMaratona;
    private String nome;
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicial;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFinal;

    private Integer numeroRegistrados;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "maratona", orphanRemoval = true)
    private List<MaratonaRound> rounds;

    @OneToMany(mappedBy = "maratona")
    @JsonIgnore
    private List<MembroMaratona> membrosMaratona;

    public Maratona() {
        rounds = new ArrayList<>();
    }

    public Long getIdMaratona() {
        return idMaratona;
    }

    public void setIdMaratona(Long idMaratona) {
        this.idMaratona = idMaratona;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Integer getNumeroRegistrados() {
        return numeroRegistrados;
    }

    public void setNumeroRegistrados(Integer numeroRegistrados) {
        this.numeroRegistrados = numeroRegistrados;
    }

    public List<MaratonaRound> getRounds() {
        return rounds;
    }

    public void setRounds(List<MaratonaRound> rounds) {
        this.rounds = rounds;
    }

    public List<MembroMaratona> getMembrosMaratona() {
        if(membrosMaratona == null){
            membrosMaratona = new ArrayList<>();
        }
        return membrosMaratona;
    }

    public void setMembrosMaratona(List<MembroMaratona> membrosMaratona) {
        this.membrosMaratona = membrosMaratona;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.Maratona[ id=" + id + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "srm")
public class Srm extends EntidadePadrao {

    private Long srmId;
    private String nome;
    private String status;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicial;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFinal;
    private Integer numeroRegistrados;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "srm", orphanRemoval = true)
    private List<SrmRound> rounds;
    @OneToMany(mappedBy = "srm")
    @JsonIgnore
    private List<MembroSrm> membroSrms;

    public Srm() {
        rounds = new ArrayList<>();
    }

    public Long getSrmId() {
        return srmId;
    }

    public void setSrmId(Long srmId) {
        this.srmId = srmId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Integer getNumeroRegistrados() {
        return numeroRegistrados;
    }

    public void setNumeroRegistrados(Integer numeroRegistrados) {
        this.numeroRegistrados = numeroRegistrados;
    }

    public List<SrmRound> getRounds() {
        return rounds;
    }

    public void setRounds(List<SrmRound> rounds) {
        this.rounds = rounds;
    }

    public List<MembroSrm> getMembroSrms() {
        return membroSrms;
    }

    public void setMembroSrms(List<MembroSrm> membroSrms) {
        this.membroSrms = membroSrms;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.Srm[ id=" + id + " ]";
    }

}

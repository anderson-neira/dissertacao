/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "conquista")
public class Conquista extends EntidadePadrao {

    private String nome;
    @Temporal(TemporalType.TIMESTAMP)
    private Date quando;
    @JsonIgnore
    @ManyToOne
    private Membro membro;

    public Conquista() {
    }

    public Conquista(Membro membro) {
        this.membro = membro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getQuando() {
        return quando;
    }

    public void setQuando(Date quando) {
        this.quando = quando;
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    @Override
    public String toString() {
        return "Conquista{" + "nome=" + nome + ", quando=" + quando +'}';
    }

}

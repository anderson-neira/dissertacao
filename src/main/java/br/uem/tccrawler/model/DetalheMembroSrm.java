/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "detalhemembrosrm")
public class DetalheMembroSrm extends EntidadePadrao {

    private Double antigaClassificacao;
    private Double novaClassificacao;
    private Integer colocacao;
    private Double totalPontos;
    private Integer colocacaoNaDivisao;
    @ManyToOne
    @JsonIgnore
    private MembroSrm membroSrm;
    @ManyToOne
    @JsonIgnore
    private SrmRound srmRound;

    public DetalheMembroSrm() {
    }

    public DetalheMembroSrm(MembroSrm membroSrm) {
        this.membroSrm = membroSrm;
    }

    public Double getAntigaClassificacao() {
        return antigaClassificacao;
    }

    public void setAntigaClassificacao(Double antigaClassificacao) {
        this.antigaClassificacao = antigaClassificacao;
    }

    public Double getNovaClassificacao() {
        return novaClassificacao;
    }

    public void setNovaClassificacao(Double novaClassificacao) {
        this.novaClassificacao = novaClassificacao;
    }

    public Integer getColocacao() {
        return colocacao;
    }

    public void setColocacao(Integer colocacao) {
        this.colocacao = colocacao;
    }

    public Double getTotalPontos() {
        return totalPontos;
    }

    public void setTotalPontos(Double totalPontos) {
        this.totalPontos = totalPontos;
    }

    public Integer getColocacaoNaDivisao() {
        return colocacaoNaDivisao;
    }

    public void setColocacaoNaDivisao(Integer colocacaoNaDivisao) {
        this.colocacaoNaDivisao = colocacaoNaDivisao;
    }

    public MembroSrm getMembroSrm() {
        return membroSrm;
    }

    public void setMembroSrm(MembroSrm membroSrm) {
        this.membroSrm = membroSrm;
    }

    public SrmRound getSrmRound() {
        return srmRound;
    }

    public void setSrmRound(SrmRound srmRound) {
        this.srmRound = srmRound;
    }

    
    @Override
    public String toString() {
        return "br.uem.tccrowler.model.DetalhesMembroSrm[ id=" + id + " ]";
    }

}

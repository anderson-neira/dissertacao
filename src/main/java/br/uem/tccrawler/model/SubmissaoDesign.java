/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "submissaodesign")
public class SubmissaoDesign extends EntidadePadrao {

    private Long idSubmissao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date quando;
    private String status;
    private Double pontuacao;
    private Integer colocao;
    @JsonIgnore
    @ManyToOne
    private MembroDesafioDesign membroDesafioDesign;

    public SubmissaoDesign() {
    }

    public SubmissaoDesign(MembroDesafioDesign membroDesafioDesign) {
        this.membroDesafioDesign = membroDesafioDesign;
    }

    public Long getIdSubmissao() {
        return idSubmissao;
    }

    public void setIdSubmissao(Long idSubmissao) {
        this.idSubmissao = idSubmissao;
    }

    public Date getQuando() {
        return quando;
    }

    public void setQuando(Date quando) {
        this.quando = quando;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(Double pontuacao) {
        this.pontuacao = pontuacao;
    }

    public Integer getColocao() {
        return colocao;
    }

    public void setColocao(Integer colocao) {
        this.colocao = colocao;
    }

    public MembroDesafioDesign getMembroDesafioDesign() {
        return membroDesafioDesign;
    }

    public void setMembroDesafioDesign(MembroDesafioDesign membroDesafioDesign) {
        this.membroDesafioDesign = membroDesafioDesign;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.SubmissaoDesign[ id=" + id + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "membrodesafiodesign")
public class MembroDesafioDesign extends EntidadePadrao {

    @ManyToOne
    @JsonIgnore
    private Membro membro;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    private DesafioDesign desafioDesign;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "membroDesafioDesign", orphanRemoval = true)
    private List<SubmissaoDesign> submissoes;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "membroDesafioDesign")
    private SubmissaoDesignVencedora submissaoDesignVencedora;

    public MembroDesafioDesign() {
    }

    public MembroDesafioDesign(Membro membro, DesafioDesign desafioDesign) {
        this.membro = membro;
        this.desafioDesign = desafioDesign;
        this.submissoes = new ArrayList<>();
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    public DesafioDesign getDesafioDesign() {
        return desafioDesign;
    }

    public void setDesafioDesign(DesafioDesign desafioDesign) {
        this.desafioDesign = desafioDesign;
    }

    public List<SubmissaoDesign> getSubmissoes() {
        return submissoes;
    }

    public void setSubmissoes(List<SubmissaoDesign> submissoes) {
        this.submissoes = submissoes;
    }

    public SubmissaoDesignVencedora getSubmissaoDesignVencedora() {
        return submissaoDesignVencedora;
    }

    public void setSubmissaoDesignVencedora(SubmissaoDesignVencedora submissaoDesignVencedora) {
        this.submissaoDesignVencedora = submissaoDesignVencedora;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.MembroDesafioDesign[ id=" + id + " ]";
    }

}

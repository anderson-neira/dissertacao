/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name="paisaux")
public class PaisAux extends EntidadePadrao {

    private int idPais;

    public PaisAux() {
    }

    public PaisAux(int idPais) {
        this.idPais = idPais;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    @Override
    public String toString() {
        return "br.uem.tccrawler.model.PaisAux[ id=" + id + " ]";
    }

}

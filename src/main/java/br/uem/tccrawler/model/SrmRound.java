/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "srmround")
public class SrmRound extends EntidadePadrao {

    private Long idRound;
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioRegisto;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fimRegistro;
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioCodificacao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fimCodificacao;
    private Long forumId;
    @ManyToOne
    @JsonIgnore
    private Srm srm;
    @OneToMany(mappedBy = "maratonaRound")
    private List<DetalheMembroMaratona> detalheMembroMaratonas;

    public SrmRound() {
        detalheMembroMaratonas = new ArrayList<>();
    }

    public SrmRound(Srm srm) {
        this.srm = srm;
        detalheMembroMaratonas = new ArrayList<>();
    }

    public Long getIdRound() {
        return idRound;
    }

    public void setIdRound(Long idRound) {
        this.idRound = idRound;
    }

    public Date getInicioRegisto() {
        return inicioRegisto;
    }

    public void setInicioRegisto(Date inicioRegisto) {
        this.inicioRegisto = inicioRegisto;
    }

    public Date getFimRegistro() {
        return fimRegistro;
    }

    public void setFimRegistro(Date fimRegistro) {
        this.fimRegistro = fimRegistro;
    }

    public Date getInicioCodificacao() {
        return inicioCodificacao;
    }

    public void setInicioCodificacao(Date inicioCodificacao) {
        this.inicioCodificacao = inicioCodificacao;
    }

    public Date getFimCodificacao() {
        return fimCodificacao;
    }

    public void setFimCodificacao(Date fimCodificacao) {
        this.fimCodificacao = fimCodificacao;
    }

    public Long getForumId() {
        return forumId;
    }

    public void setForumId(Long forumId) {
        this.forumId = forumId;
    }

    public Srm getSrm() {
        return srm;
    }

    public void setSrm(Srm srm) {
        this.srm = srm;
    }

    public List<DetalheMembroMaratona> getDetalheMembroMaratonas() {
        return detalheMembroMaratonas;
    }

    public void setDetalheMembroMaratonas(List<DetalheMembroMaratona> detalheMembroMaratonas) {
        this.detalheMembroMaratonas = detalheMembroMaratonas;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.SrmRound[ id=" + id + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "habilidade")
public class Habilidade extends EntidadePadrao {

    private String nome;
    
    public Habilidade() {
    }

    public Habilidade(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }  

    @Override
    public String toString() {
        return "Habilidade{" + "nome=" + nome + '}';
    }

}

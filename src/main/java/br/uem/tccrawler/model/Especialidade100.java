/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import br.uem.tccrawler.service.MembroService;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "especialidade100")
public class Especialidade100 extends EntidadePadrao {

    @ManyToOne
    private Membro membro;
    private String tecnologia;

    public Especialidade100() {
    }

    public Especialidade100(String nomeMembro, String nomeTecnologia) {

        membro = MembroService.buscaPorNome(nomeMembro);
        tecnologia = nomeTecnologia;
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    public String getTecnologia() {
        return tecnologia;
    }

    public void setTecnologia(String tecnologia) {
        this.tecnologia = tecnologia;
    }
    
}

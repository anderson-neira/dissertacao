/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "detalhemembromaratona")
public class DetalheMembroMaratona extends EntidadePadrao {

    private Double antigaPontuacao;
    private Double novaPontuacao;
    private Integer colocacao;
    private Double totalPontos;
    private Integer classificacaoGeral;
    private Integer numSubmissoes;
    @ManyToOne
    @JsonIgnore
    private MembroMaratona membroMaratona;
    @ManyToOne
    @JsonIgnore
    private MaratonaRound maratonaRound;

    public DetalheMembroMaratona() {
    }

    public DetalheMembroMaratona(MembroMaratona membroMaratona) {
        this.membroMaratona = membroMaratona;
    }

    public Double getAntigaPontuacao() {
        return antigaPontuacao;
    }

    public void setAntigaPontuacao(Double antigaPontuacao) {
        this.antigaPontuacao = antigaPontuacao;
    }

    public Double getNovaPontuacao() {
        return novaPontuacao;
    }

    public void setNovaPontuacao(Double novaPontuacao) {
        this.novaPontuacao = novaPontuacao;
    }

    public Integer getColocacao() {
        return colocacao;
    }

    public void setColocacao(Integer colocacao) {
        this.colocacao = colocacao;
    }

    public Double getTotalPontos() {
        return totalPontos;
    }

    public void setTotalPontos(Double totalPontos) {
        this.totalPontos = totalPontos;
    }

    public Integer getClassificacaoGeral() {
        return classificacaoGeral;
    }

    public void setClassificacaoGeral(Integer classificacaoGeral) {
        this.classificacaoGeral = classificacaoGeral;
    }

    public Integer getNumSubmissoes() {
        return numSubmissoes;
    }

    public void setNumSubmissoes(Integer numSubmissoes) {
        this.numSubmissoes = numSubmissoes;
    }

    public MembroMaratona getMembroMaratona() {
        return membroMaratona;
    }

    public void setMembroMaratona(MembroMaratona membroMaratona) {
        this.membroMaratona = membroMaratona;
    }

    public MaratonaRound getMaratonaRound() {
        return maratonaRound;
    }

    public void setMaratonaRound(MaratonaRound maratonaRound) {
        this.maratonaRound = maratonaRound;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.DeatlhesMembroMaratona[ id=" + id + " ]";
    }

}

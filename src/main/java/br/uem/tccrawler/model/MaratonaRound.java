/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "maratonaround")
public class MaratonaRound extends EntidadePadrao {

    private Long idRound;
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioRegisto;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fimRegistro;
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioCodificacao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fimCodificacao;
    private Long forumId;
    @JsonIgnore
    @ManyToOne
    private Maratona maratona;

    public MaratonaRound() {
    }

    public MaratonaRound(Maratona maratona) {
        this.maratona = maratona;
    }

    public Long getIdRound() {
        return idRound;
    }

    public void setIdRound(Long idRound) {
        this.idRound = idRound;
    }

    public Date getInicioRegisto() {
        return inicioRegisto;
    }

    public void setInicioRegisto(Date inicioRegisto) {
        this.inicioRegisto = inicioRegisto;
    }

    public Date getFimRegistro() {
        return fimRegistro;
    }

    public void setFimRegistro(Date fimRegistro) {
        this.fimRegistro = fimRegistro;
    }

    public Date getInicioCodificacao() {
        return inicioCodificacao;
    }

    public void setInicioCodificacao(Date inicioCodificacao) {
        this.inicioCodificacao = inicioCodificacao;
    }

    public Date getFimCodificacao() {
        return fimCodificacao;
    }

    public void setFimCodificacao(Date fimCodificacao) {
        this.fimCodificacao = fimCodificacao;
    }

    public Long getForumId() {
        return forumId;
    }

    public void setForumId(Long forumId) {
        this.forumId = forumId;
    }

    public Maratona getMaratona() {
        return maratona;
    }

    public void setMaratona(Maratona maratona) {
        this.maratona = maratona;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.Round[ id=" + id + " ]";
    }

}

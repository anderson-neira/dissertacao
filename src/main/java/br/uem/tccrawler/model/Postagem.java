/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name="postagem")
public class Postagem extends EntidadePadrao {

    @ManyToOne
    private Forum forum;
    @ManyToOne
    private Membro membro;
    @Column(columnDefinition = "text")
    private String descricao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date quando;
    private int ordem;

    public Postagem() {
    }

    public Postagem(Forum forum, int ordem) {
        this.forum = forum;
        this.ordem = ordem;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getQuando() {
        return quando;
    }

    public void setQuando(Date quando) {
        this.quando = quando;
    }

    public int getOrdem() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    @Override
    public String toString() {
        return "Postagem{" + "forum=" + forum.getId()  + ", descricao=" + descricao + ", quando=" + quando + ", ordem=" + ordem + '}';
    }

   

}

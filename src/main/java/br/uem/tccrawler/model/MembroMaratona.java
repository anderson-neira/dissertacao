/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "membromaratona")
public class MembroMaratona extends EntidadePadrao {

    @ManyToOne
    @JsonIgnore
    private Membro membro;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    private Maratona maratona;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "membroMaratona", orphanRemoval = true)
    private List<DetalheMembroMaratona> detalhesMembroMaratona;

    public MembroMaratona() {
    } 

    public MembroMaratona(Membro membro, Maratona maratona) {
        this.membro = membro;
        this.maratona = maratona;
        this.detalhesMembroMaratona = new ArrayList<>();
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    public Maratona getMaratona() {
        return maratona;
    }

    public void setMaratona(Maratona maratona) {
        this.maratona = maratona;
    }

    public List<DetalheMembroMaratona> getDetalhesMembroMaratona() {
        return detalhesMembroMaratona;
    }

    public void setDetalhesMembroMaratona(List<DetalheMembroMaratona> detalhesMembroMaratona) {
        this.detalhesMembroMaratona = detalhesMembroMaratona;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.MembroMaratona[ id=" + id + " ]";
    }

}

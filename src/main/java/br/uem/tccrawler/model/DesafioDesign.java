/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "desafiodesign")
public class DesafioDesign extends EntidadePadrao {

    @Temporal(TemporalType.TIMESTAMP)
    private Date atualizado;
    @Temporal(TemporalType.TIMESTAMP)
    private Date criado;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Tecnologia> tecnologias;
    private String subGrupo;
    private String nome;
    private Long idDesafio;
    private Long forumId;
    private Integer numSubmissoes;
    private Integer numRegistrados;
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioRegistro;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fimRegistro;
    @Temporal(TemporalType.TIMESTAMP)
    private Date finalSubmissao;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Plataforma> plataformas;
    private BigDecimal totalRecompensa;   
    @OneToMany(mappedBy = "desafioDesign")
    @JsonIgnore
    private List<MembroDesafioDesign> membroDesafiosDesign;


    public DesafioDesign() {
    }

    public DesafioDesign(Long idDesafio) {
        tecnologias = new ArrayList<>();
        plataformas = new ArrayList<>();
        membroDesafiosDesign = new ArrayList<>();
        this.idDesafio = idDesafio;
    }

    public Date getAtualizado() {
        return atualizado;
    }

    public void setAtualizado(Date atualizado) {
        this.atualizado = atualizado;
    }

    public Date getCriado() {
        return criado;
    }

    public void setCriado(Date criado) {
        this.criado = criado;
    }

    public List<Tecnologia> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<Tecnologia> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getSubGrupo() {
        return subGrupo;
    }

    public void setSubGrupo(String subGrupo) {
        this.subGrupo = subGrupo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getForumId() {
        return forumId;
    }

    public void setForumId(Long forumId) {
        this.forumId = forumId;
    }

    public Integer getNumSubmissoes() {
        return numSubmissoes;
    }

    public void setNumSubmissoes(Integer numSubmissoes) {
        this.numSubmissoes = numSubmissoes;
    }

    public Integer getNumRegistrados() {
        return numRegistrados;
    }

    public void setNumRegistrados(Integer numRegistrados) {
        this.numRegistrados = numRegistrados;
    }

    public Date getFimRegistro() {
        return fimRegistro;
    }

    public void setFimRegistro(Date fimRegistro) {
        this.fimRegistro = fimRegistro;
    }

    public Date getFinalSubmissao() {
        return finalSubmissao;
    }

    public void setFinalSubmissao(Date finalSubmissao) {
        this.finalSubmissao = finalSubmissao;
    }

    public List<Plataforma> getPlataformas() {
        return plataformas;
    }

    public void setPlataformas(List<Plataforma> plataformas) {
        this.plataformas = plataformas;
    }

    public BigDecimal getTotalRecompensa() {
        return totalRecompensa;
    }

    public void setTotalRecompensa(BigDecimal totalRecompensa) {
        this.totalRecompensa = totalRecompensa;
    }

    public List<MembroDesafioDesign> getMembroDesafiosDesign() {
        return membroDesafiosDesign;
    }

    public void setMembroDesafiosDesign(List<MembroDesafioDesign> membroDesafiosDesign) {
        this.membroDesafiosDesign = membroDesafiosDesign;
    }

    public Long getIdDesafio() {
        return idDesafio;
    }

    public void setIdDesafio(Long idDesafio) {
        this.idDesafio = idDesafio;
    }

    public Date getInicioRegistro() {
        return inicioRegistro;
    }

    public void setInicioRegistro(Date inicioRegistro) {
        this.inicioRegistro = inicioRegistro;
    }   

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.DesafioDesign[ id=" + id + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "membrosrm")
public class MembroSrm extends EntidadePadrao {

    @ManyToOne
    @JsonIgnore
    private Membro membro;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    private Srm srm;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "membroSrm", orphanRemoval = true)
    private List<DetalheMembroSrm> detalhesMembroSrm;

    public MembroSrm() {
    }

    public MembroSrm(Membro membro, Srm srm) {
        this.membro = membro;
        this.srm = srm;
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    public Srm getSrm() {
        return srm;
    }

    public void setSrm(Srm srm) {
        this.srm = srm;
    }

    public List<DetalheMembroSrm> getDetalhesMembroSrm() {
        return detalhesMembroSrm;
    }

    public void setDetalhesMembroSrm(List<DetalheMembroSrm> detalhesMembroSrm) {
        this.detalhesMembroSrm = detalhesMembroSrm;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.MembroSrm[ id=" + id + " ]";
    }

}

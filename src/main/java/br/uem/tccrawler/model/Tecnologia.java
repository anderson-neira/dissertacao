/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "tecnologia")
public class Tecnologia extends EntidadePadrao {

    private String nome;
    private String nomeAux;

    private String opcao03;

    public Tecnologia() {
    }

    public Tecnologia(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeAux() {
        return nomeAux;
    }

    public void setNomeAux(String nomeAux) {
        this.nomeAux = nomeAux;
    }

    public String getOpcao03() {
        return opcao03;
    }

    public void setOpcao03(String opcao03) {
        this.opcao03 = opcao03;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.Tecnologia[ id=" + id + " ]";
    }

}

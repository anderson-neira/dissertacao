/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "ultimasubmissao")
public class UltimaSubmissao extends EntidadePadrao {

    private String tipo;
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaSubmissao;
    @JsonIgnore
    @ManyToOne
    private Membro membro;

    public UltimaSubmissao() {
    }

 

    public UltimaSubmissao(String tipo, Membro membro) {
        this.tipo = tipo;
        this.membro = membro;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getUltimaSubmissao() {
        return ultimaSubmissao;
    }

    public void setUltimaSubmissao(Date ultimaSubmissao) {
        this.ultimaSubmissao = ultimaSubmissao;
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.UltimaSubmissao[ id=" + id + " ]";
    }

}

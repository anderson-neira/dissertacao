/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "submissaodesignvencedora")
public class SubmissaoDesignVencedora extends EntidadePadrao {

    private Long idSubimissaoVencedora;
    private BigDecimal recompensaGanha;
    private int classificacao;
    private Double pontuacaoFinal;
    @JsonIgnore
    @OneToOne
    private MembroDesafioDesign membroDesafioDesign;

    public SubmissaoDesignVencedora() {
    }

    public SubmissaoDesignVencedora(MembroDesafioDesign membroDesafioDesign) {
        this.membroDesafioDesign = membroDesafioDesign;
    }

    public Long getIdSubimissaoVencedora() {
        return idSubimissaoVencedora;
    }

    public void setIdSubimissaoVencedora(Long idSubimissaoVencedora) {
        this.idSubimissaoVencedora = idSubimissaoVencedora;
    }

    public BigDecimal getRecompensaGanha() {
        return recompensaGanha;
    }

    public void setRecompensaGanha(BigDecimal recompensaGanha) {
        this.recompensaGanha = recompensaGanha;
    }

    public int getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

    public Double getPontuacaoFinal() {
        return pontuacaoFinal;
    }

    public void setPontuacaoFinal(Double pontuacaoFinal) {
        this.pontuacaoFinal = pontuacaoFinal;
    }

    public MembroDesafioDesign getMembroDesafioDesign() {
        return membroDesafioDesign;
    }

    public void setMembroDesafioDesign(MembroDesafioDesign membroDesafioDesign) {
        this.membroDesafioDesign = membroDesafioDesign;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.SubmissaoDesignVencedora[ id=" + id + " ]";
    }

}

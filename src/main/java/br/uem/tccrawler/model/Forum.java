/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "forum")
public class Forum extends EntidadePadrao {

    private String idTopcoder;
    private String nome;
    private String subCategoria;
    private String categoria;
    @ManyToOne
    private Membro autor;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "forum")
    private List<Postagem> postagem;

    public Forum() {
        postagem = new ArrayList<>();
    }

    public Forum(String idTopcoder) {
        this.idTopcoder = idTopcoder;
    }

    public String getIdTopcoder() {
        return idTopcoder;
    }

    public void setIdTopcoder(String idTopcoder) {
        this.idTopcoder = idTopcoder;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(String subCategoria) {
        this.subCategoria = subCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Membro getAutor() {
        return autor;
    }

    public void setAutor(Membro autor) {
        this.autor = autor;
    }

    public List<Postagem> getPostagem() {
        return postagem;
    }

    public void setPostagem(List<Postagem> postagem) {
        this.postagem = postagem;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.Forumm[ id=" + id + " ]";
    }

}

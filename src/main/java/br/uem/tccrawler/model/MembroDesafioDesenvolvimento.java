/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "membrodesafiodesenvolvimento")
public class MembroDesafioDesenvolvimento extends EntidadePadrao {

    @ManyToOne
    @JsonIgnore
    private Membro membro;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    private DesafioDesenvolvimento desafioDesenvolvimento;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "membroDesafioDesenvolvimento", orphanRemoval = true)
    private List<SubmissaoDesenvolvimento> submissoes;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "desafioDesenvolvimento")
    private SubmissaoDesenvolvimentoVencedora submissaoDesenvolvimentoVencedora;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            joinColumns = {
                @JoinColumn(name = "membrodesafiodesenvolvimento_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "regras_id")},
            uniqueConstraints = {
                @UniqueConstraint(columnNames = {"membrodesafiodesenvolvimento_id", "regras_id"})}
    )

    private List<Regra> regras;
    private boolean temSubmisao;
    private boolean participouComoSubmiter;

    public MembroDesafioDesenvolvimento() {
    }

    public MembroDesafioDesenvolvimento(Membro membro, DesafioDesenvolvimento desafioDesenvolvimento) {
        this.membro = membro;
        this.desafioDesenvolvimento = desafioDesenvolvimento;
        this.submissoes = new ArrayList<>();
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    public DesafioDesenvolvimento getDesafioDesenvolvimento() {
        return desafioDesenvolvimento;
    }

    public void setDesafioDesenvolvimento(DesafioDesenvolvimento desafioDesenvolvimento) {
        this.desafioDesenvolvimento = desafioDesenvolvimento;
    }

    public List<SubmissaoDesenvolvimento> getSubmissoes() {
        return submissoes;
    }

    public void setSubmissoes(List<SubmissaoDesenvolvimento> submissoes) {
        this.submissoes = submissoes;
    }

    public SubmissaoDesenvolvimentoVencedora getSubmissaoDesenvolvimentoVencedora() {
        return submissaoDesenvolvimentoVencedora;
    }

    public void setSubmissaoDesenvolvimentoVencedora(SubmissaoDesenvolvimentoVencedora submissaoDesenvolvimentoVencedora) {
        this.submissaoDesenvolvimentoVencedora = submissaoDesenvolvimentoVencedora;
    }

    public List<Regra> getRegras() {
        return regras;
    }

    public void setRegras(List<Regra> regras) {
        this.regras = regras;
    }

    public boolean isTemSubmisao() {
        return temSubmisao;
    }

    public void setTemSubmisao(boolean temSubmisao) {
        this.temSubmisao = temSubmisao;
    }

    @Override
    public String toString() {
        return "br.uem.tccrowler.model.MembroDesafioDesenvolvimento[ id=" + id + " ]";
    }

}

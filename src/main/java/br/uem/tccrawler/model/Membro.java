/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uem.tccrawler.model;

import br.com.munif.entidades.EntidadePadrao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author anderson
 */
@Entity
@Table(name = "membro")
public class Membro extends EntidadePadrao {

    //pegar do link https://api.topcoder.com/v2/users/andersonneira/stats/
    private String nome; // apelido
    private String pais;
    @Temporal(TemporalType.TIMESTAMP)
    private Date membroDesde;
    private boolean copiloto;

    //pegar do link https://api.topcoder.com/v3/members/andersonneira/stats/
    private Long userId;
    private int qtdeDesafio;
    private int qtdeVitoria;
    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<UltimaSubmissao> ultimasSubmissoes;
    private String linguagemPadraoSrm;

    // pegar as habilidades https://api.topcoder.com/v3/members/andersonneira/skills/
//    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private List<Habilidade> habilidades;

    // pegar do link https://api.topcoder.com/v3/members/monicamuranyi/
    private String status;
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimoUpdateConta;
//    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private List<Track> tracks;

    // pegar do link  https://api.topcoder.com/v3/members/andersonneira/challenges/?filter=track%3dDEVELOP&limit=50&offset=150
    private int qtdeDesafiosDesenvolvimento;
//    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<MembroDesafioDesenvolvimento> membroDesafiosDesenvolvimento;

    //pegar do link https://api.topcoder.com/v3/members/monicamuranyi/challenges/?filter=track%3DDESIGN&limit=50
    private int qtdeDesafiosDesign;
//    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<MembroDesafioDesign> membroDesafiosDesign;

    //pegar do link https://api.topcoder.com/v3/members/shangjingbo/mms/?orderBy=endDate+desc
    private int qtdeMaratonas;
//    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<MembroMaratona> membrosMaratona;

    //pegar do link https://api.topcoder.com/v3/members/wleite/srms/?limit=50&offset=50&orderBy=endDate+desc
    private int qtdeSrms;
//    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<MembroSrm> membroSrms;

    //pegar os Achievements https://api.topcoder.com/v2/users/MonicaMuranyi
//    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Conquista> conquistas;

    @OneToMany(mappedBy = "membro", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Postagem> postagem;

//    @Temporal(TemporalType.TIMESTAMP)
//    private Date primeiroDesafio;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date membroDesde6Meses;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date membroDesde12Meses;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date membroDesde18Meses;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date primeiroDesafioDevDes;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date primeiroDesafioNovo;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date primeiroDesafioNovo2;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataPrimeiroDesafio; // somente de dev
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataPrimeiroDesafioOutros; // design srm e marathon

    private int qtdeParticipacoesDev;

    private boolean temMais75;
//    private boolean temMais90;
//    private boolean temMais100;

    public Membro() {
    }

    public Membro(String nome) {
        novo = false;
        this.nome = nome;
        ultimasSubmissoes = new ArrayList<>();
        tracks = new ArrayList<>();
        habilidades = new ArrayList<>();
        membroDesafiosDesenvolvimento = new ArrayList<>();
        membroDesafiosDesign = new ArrayList<>();
        membroSrms = new ArrayList<>();
        membrosMaratona = new ArrayList<>();
        conquistas = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Date getMembroDesde() {
        return membroDesde;
    }

    public void setMembroDesde(Date membroDesde) {
        this.membroDesde = membroDesde;
    }

    public boolean isCopiloto() {
        return copiloto;
    }

    public void setCopiloto(boolean copiloto) {
        this.copiloto = copiloto;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getQtdeDesafio() {
        return qtdeDesafio;
    }

    public void setQtdeDesafio(int qtdeDesafio) {
        this.qtdeDesafio = qtdeDesafio;
    }

    public int getQtdeVitoria() {
        return qtdeVitoria;
    }

    public void setQtdeVitoria(int qtdeVitoria) {
        this.qtdeVitoria = qtdeVitoria;
    }

    public List<UltimaSubmissao> getUltimasSubmissoes() {
        return ultimasSubmissoes;
    }

    public void setUltimasSubmissoes(List<UltimaSubmissao> ultimasSubmissoes) {
        this.ultimasSubmissoes = ultimasSubmissoes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUltimoUpdateConta() {
        return ultimoUpdateConta;
    }

    public void setUltimoUpdateConta(Date ultimoUpdateConta) {
        this.ultimoUpdateConta = ultimoUpdateConta;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    public List<Habilidade> getHabilidades() {
        return habilidades;
    }

    public void setHabilidades(List<Habilidade> habilidades) {
        this.habilidades = habilidades;
    }

    public int getQtdeDesafiosDesenvolvimento() {
        return qtdeDesafiosDesenvolvimento;
    }

    public void setQtdeDesafiosDesenvolvimento(int qtdeDesafiosDesenvolvimento) {
        this.qtdeDesafiosDesenvolvimento = qtdeDesafiosDesenvolvimento;
    }

    public List<MembroDesafioDesenvolvimento> getMembroDesafiosDesenvolvimento() {
        return membroDesafiosDesenvolvimento;
    }

    public void setMembroDesafiosDesenvolvimento(List<MembroDesafioDesenvolvimento> membroDesafiosDesenvolvimento) {
        this.membroDesafiosDesenvolvimento = membroDesafiosDesenvolvimento;
    }

    public int getQtdeDesafiosDesign() {
        return qtdeDesafiosDesign;
    }

    public void setQtdeDesafiosDesign(int qtdeDesafiosDesign) {
        this.qtdeDesafiosDesign = qtdeDesafiosDesign;
    }

    public List<MembroDesafioDesign> getMembroDesafiosDesign() {
        return membroDesafiosDesign;
    }

    public void setMembroDesafiosDesign(List<MembroDesafioDesign> membroDesafiosDesign) {
        this.membroDesafiosDesign = membroDesafiosDesign;
    }

    public int getQtdeMaratonas() {
        return qtdeMaratonas;
    }

    public void setQtdeMaratonas(int qtdeMaratonas) {
        this.qtdeMaratonas = qtdeMaratonas;
    }

    public List<MembroMaratona> getMembrosMaratona() {
        return membrosMaratona;
    }

    public void setMembrosMaratona(List<MembroMaratona> membrosMaratona) {
        this.membrosMaratona = membrosMaratona;
    }

    public int getQtdeSrms() {
        return qtdeSrms;
    }

    public void setQtdeSrms(int qtdeSrms) {
        this.qtdeSrms = qtdeSrms;
    }

    public String getLinguagemPadraoSrm() {
        return linguagemPadraoSrm;
    }

    public void setLinguagemPadraoSrm(String linguagemPadraoSrm) {
        this.linguagemPadraoSrm = linguagemPadraoSrm;
    }

    public List<MembroSrm> getMembroSrms() {
        return membroSrms;
    }

    public void setMembroSrms(List<MembroSrm> membroSrms) {
        this.membroSrms = membroSrms;
    }

    public List<Conquista> getConquistas() {
        return conquistas;
    }

    public void setConquistas(List<Conquista> conquistas) {
        this.conquistas = conquistas;
    }

    public List<Postagem> getPostagem() {
        return postagem;
    }

    public void setPostagem(List<Postagem> postagem) {
        this.postagem = postagem;
    }

    public int getQtdeParticipacoesDev() {
        return qtdeParticipacoesDev;
    }

    public void setQtdeParticipacoesDev(int qtdeParticipacoesDev) {
        this.qtdeParticipacoesDev = qtdeParticipacoesDev;
    }

    /**
     * Metodo que soma a quantidade todos os dasafios (srms, maratonas, desen,
     * desing).
     *
     * @return somaDesafios
     */
    public int totalDesafios() {
        int retornar = 0;
        if (membroDesafiosDesenvolvimento != null) {
            retornar = retornar + membroDesafiosDesenvolvimento.size();
        }
        if (membroDesafiosDesign != null) {
            retornar = retornar + membroDesafiosDesign.size();
        }
        if (membroSrms != null) {
            retornar = retornar + membroSrms.size();
        }
        if (membrosMaratona != null) {
            retornar = retornar + membrosMaratona.size();
        }
        return retornar;
    }

    @Override
    public String toString() {
        return "Membro{" + "nome=" + nome + ", pais=" + pais + ", membroDesde=" + membroDesde + ", copiloto=" + copiloto + ", userId=" + userId + ", qtdeDesafio=" + qtdeDesafio + ", qtdeVitoria=" + qtdeVitoria + ", ultimasSubmissoes=" + ultimasSubmissoes + ", linguagemPadraoSrm=" + linguagemPadraoSrm + ", habilidades=" + habilidades + ", status=" + status + ", ultimoUpdateConta=" + ultimoUpdateConta + ", tracks=" + tracks + ", qtdeDesafiosDesenvolvimento=" + qtdeDesafiosDesenvolvimento + ", membroDesafiosDesenvolvimento=" + membroDesafiosDesenvolvimento + ", qtdeDesafiosDesign=" + qtdeDesafiosDesign + ", membroDesafiosDesign=" + membroDesafiosDesign + ", qtdeMaratonas=" + qtdeMaratonas + ", membrosMaratona=" + membrosMaratona + ", qtdeSrms=" + qtdeSrms + ", membroSrms=" + membroSrms + ", conquistas=" + conquistas + '}';
    }

}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.util;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.DadosAuditoria;
import br.com.munif.util.web.MuniFilter;
import br.uem.tccrawler.model.Membro;
import br.uem.tccrawler.model.NomeMembro;
import br.uem.tccrawler.service.MembroService;
import br.uem.tccrawler.service.NomeMembroCrawler3;
import br.uem.tccrawler.service.NomeMembroService;
import br.uem.tccrawler.service.TecnologiaService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author anderson
 */
public class BuscaNomes {

    private static ThreadLocal<DadosAuditoria> tlda = new ThreadLocal<>();

    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.println("Testando");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("UnidadeDePersistencia");
        EntityManager entityManager = emf.createEntityManager();
        MuniFilter.setEntityManagerCorrente(entityManager);
        DadosAuditoria da = new DadosAuditoria(null, null); // setar a auditoria para não quebrar
        tlda.set(da);

        buscar(entityManager);
    }

    private static void buscar(EntityManager entityManager) {

        String nome = "maydayuiui";

        long inicio = System.currentTimeMillis();
        entityManager.getTransaction().begin();
        Membro m = new Membro(nome);
        try {
            m = MembroService.buscaMembroNaAPI(m);
        } catch (Exception e) {
            System.out.println("Deu erro " + nome);
            e.printStackTrace();

        }
        GenericCrudBean.getInstance().inserir(m);
        System.out.println("terminou o membro: " + nome + " em " + (System.currentTimeMillis() - inicio));
        entityManager.getTransaction().commit();

    }

}

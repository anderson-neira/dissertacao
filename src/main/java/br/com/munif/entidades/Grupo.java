
package br.com.munif.entidades;

import br.com.munif.util.web.TraduzJSF;

public enum Grupo {

    ADMIN("administrador"),
    CANDIDATO("candidato"),    
    ;
    
    private String descricao;

    private Grupo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return TraduzJSF.getMsgsTraduzidas(this.descricao);
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}

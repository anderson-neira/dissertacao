/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;

/**
 *
 * @author munif
 */
@Entity
public class Arquivo extends EntidadePadrao {
    @JsonIgnore
    private String nome;
    @JsonIgnore
    private String mimeType;
    @JsonIgnore
    private Long tamanho;
    @JsonIgnore
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date momentoDoUpload;
    @JsonIgnore
    @OrderBy(value = "id")
    @OneToMany(mappedBy = "arquivo", cascade = CascadeType.ALL)
    private List<ArquivoParte> partes;
    @JsonIgnore
    @Lob
    @Column(length = 1024 * 1024) //Principalmente por causa do Mysql
    private byte thumb[];
    @JsonIgnore
    @Lob
    @Column(length = 1024 * 1024) //Principalmente por causa do Mysql
    private byte webthumb[];
    

    public byte[] getWebthumb() {
        return webthumb;
    }

    public void setWebthumb(byte[] webthumb) {
        this.webthumb = webthumb;
    }

    public Arquivo() {
        momentoDoUpload = new Date();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Long getTamanho() {
        return tamanho;
    }

    public void setTamanho(Long tamanho) {
        this.tamanho = tamanho;
    }

    public Date getMomentoDoUpload() {
        return momentoDoUpload;
    }

    public void setMomentoDoUpload(Date momentoDoUpload) {
        this.momentoDoUpload = momentoDoUpload;
    }

    public List<ArquivoParte> getPartes() {
        return partes;
    }

    public void setPartes(List<ArquivoParte> partes) {
        this.partes = partes;
    }

    public byte[] getThumb() {
        return thumb;
    }

    public void setThumb(byte[] thumb) {
        this.thumb = thumb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Arquivo)) {
            return false;
        }
        Arquivo other = (Arquivo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Arquivo com id " + getId();
    }
}

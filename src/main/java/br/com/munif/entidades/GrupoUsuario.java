
package br.com.munif.entidades;

import javax.persistence.*;
import org.hibernate.envers.Audited;

@Audited
@Entity
public class GrupoUsuario extends EntidadePadrao {

    private static final long serialVersionUID = 1L;
    @Enumerated(EnumType.STRING)
    private Grupo grupo;
    @ManyToOne
    private Usuario usuario;

    public GrupoUsuario() {
    }


    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return grupo.getDescricao();
    }
}

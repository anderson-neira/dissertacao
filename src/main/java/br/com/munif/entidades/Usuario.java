/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.entidades;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import org.hibernate.envers.Audited;

/**
 *
 * @author Peixe
 */
@Audited
@Entity
public class Usuario extends EntidadePadrao {

    private static final long serialVersionUID = 1L;
    @Column(unique = true)
    private String login;
    private String senha;
    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<GrupoUsuario> grupoUsuario;
    private Boolean ativo;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Transient
    private Date lastLoginScreen;

    public Usuario() {
        grupoUsuario = new ArrayList<GrupoUsuario>();
        ativo = Boolean.TRUE;
    }

    public Usuario(String login) {
        this.login = login;
    }
 
    public List<GrupoUsuario> getGrupoUsuario() {
        return grupoUsuario;
    }

    public void setGrupoUsuario(List<GrupoUsuario> grupoUsuario) {
        this.grupoUsuario = grupoUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public String toString() {
        return login;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLoginScreen = this.lastLogin;
        this.lastLogin = lastLogin;
    }

    public Date getLastLoginScreen() {
        return lastLoginScreen;
    }

    public void setLastLoginScreen(Date lastLoginScreen) {
        this.lastLoginScreen = lastLoginScreen;
    }

    public boolean temEsseGrupo(Grupo grupo) {
        for (GrupoUsuario g : grupoUsuario) {
            if (g.getGrupo().equals(grupo)) {
                return true;
            }
        }
        return false;
    }
    
//    public boolean isValet() {
//        for (GrupoUsuario g : grupoUsuario) {
//            if (g.getGrupo().equals(Grupo.VALET)) {
//                return true;
//            }
//        }
//        return false;
//    }
//    
//    public boolean isEstabelecimento() {
//        for (GrupoUsuario g : grupoUsuario) {
//            if (g.getGrupo().equals(Grupo.ESTABELECIMENTO)) {
//                return true;
//            }
//        }
//        return false;
//    }
}

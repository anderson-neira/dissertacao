/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
 *
 * @author munif
 *
 * ESSA ENTIDADE OBRIGATÓRIAMENTE TEM QUE SER implements Serializable E NÃO
 * extends EntiadePadrão. Se for esta for extendidada de EntidadePadrao O ID não
 * fica necessáriamente em ordem quando são inseridos vários ao mesmo tempo Isso
 * quebra as imagens pois os blocos ficam fora de ordem
 *
 */
@Entity
public class ArquivoParte implements Serializable {

    public static final int TAMANHO_MAXIMO = 4096;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Lob
    @Column(length = TAMANHO_MAXIMO) //Principalmente por causa do Mysql
    private byte dados[];
    @ManyToOne
    private Arquivo arquivo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getDados() {
        return dados;
    }

    public void setDados(byte[] dados) {
        this.dados = dados;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    @Override
    public String toString() {
        return "ArquivoParte[ id=" + id + " ]";
    }
}

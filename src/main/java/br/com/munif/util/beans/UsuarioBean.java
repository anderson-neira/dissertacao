package br.com.munif.util.beans;


import br.com.munif.entidades.Usuario;
import br.com.munif.util.web.MuniFilter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author munif
 */
public class UsuarioBean {

    private UsuarioBean() {
    }

    public static Usuario recupera(String login, String senha) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Usuario usu where usu.login=:login and usu.senha=:senha");
        q.setParameter("login", login);
        q.setParameter("senha", senha);
        List<Usuario> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        Usuario usu = resultList.get(0);
        if (usu.getSenha().equals(senha)) {
            return usu;
        }
        return null;
    }

    public static Usuario recupera(String primeiroAtributo) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Usuario usu where usu.login=:login");
        q.setParameter("login", primeiroAtributo);
        List<Usuario> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        return resultList.get(0);
    }

    public static List<Usuario> lista() {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Usuario obj order by obj.login");
        q.setMaxResults(GenericCrudBean.MAXCONSULTA + 1);
        if(q.getResultList().isEmpty()){
            return null;
        }
        return q.getResultList();
    }

    public static List<Usuario> listaFiltrando(String filtro) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Usuario obj where lower(obj.login) like lower(:filtro) order by obj.login");
        q.setParameter("filtro", "%" + filtro + "%");
        q.setMaxResults(GenericCrudBean.MAXCONSULTA + 1);
        return q.getResultList();
    }

  


    /**
     *
     * Metodo usado para validar o cadastro de um novo usuario, pois não pode
     * deixar inserir dois usuarios com o mesmo nome, entao este metodo verifica
     * se existe um usuario que já possuia o nome
     *
     * @param nome compara como o login do usuario;
     *
     * @return retorna verdadario se já existes um usuario como nome escolhido e
     * NÃO DEIXA CADASTRAR UM NOVO. Retorna false se não encontra nenhum usuario
     * com o nome escolhido, PODENDO ENTAO CADASTRAR UM NOVO USAURIO;
     *
     */
    public static Boolean verificaNomeUsuario(String nome) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from Usuario obj "
                + " where lower(obj.login) like lower(:filtro) ");
        q.setParameter("filtro", nome);
        q.setMaxResults(1);
        return !q.getResultList().isEmpty();        
    }

}

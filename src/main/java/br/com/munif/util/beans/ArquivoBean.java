/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.beans;

import br.com.munif.entidades.Arquivo;
import br.com.munif.entidades.ArquivoParte;
import br.com.munif.util.Util;
import br.com.munif.util.web.MuniFilter;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author munif
 */
public class ArquivoBean {

    private static final ArquivoBean instance = new ArquivoBean();

    public static ArquivoBean getInstance() {
        return instance;
    }

    private ArquivoBean() {
    }

    public static void adicionaArquivo(Arquivo arquivo, InputStream inputstream) throws IOException {
        /*EntityManager em = Persistencia.getTlem().get();
         em.persist(arquivo);
         int t = 0;
         try {
         while ((t = inputstream.available()) > 0) {
         byte buffer[]; //byte buffer[]=new byte[t>ArquivoParte.TAMANHO_MAXIMO?ArquivoParte.TAMANHO_MAXIMO:t];  OPERADOR TERNÁRIO
         if (t > ArquivoParte.TAMANHO_MAXIMO) {
         buffer = new byte[ArquivoParte.TAMANHO_MAXIMO];
         } else {
         buffer = new byte[t];
         }
         if (buffer.length != inputstream.read(buffer)) {
         throw new RuntimeException("Lido com erro!");
         }
         ArquivoParte arquivoParte = new ArquivoParte();
         arquivoParte.setDados(buffer);
         arquivoParte.setArquivo(arquivo);
         em.persist(arquivoParte);
         }
         } catch (Exception ex) {
         em.getTransaction().rollback();
         ex.printStackTrace();
         }*/

        String tipo = arquivo.getMimeType();

        EntityManager em = MuniFilter.getEntityManagerCorrente();
        int bytesLidos = 0;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while (true) {
            int restante = inputstream.available();
            byte[] buffer = new byte[restante > ArquivoParte.TAMANHO_MAXIMO ? ArquivoParte.TAMANHO_MAXIMO : restante];
            bytesLidos = inputstream.read(buffer);
            if (bytesLidos <= 0) {
                break;
            }
            ArquivoParte arquivoParte = new ArquivoParte();
            arquivoParte.setDados(buffer);
            arquivoParte.setArquivo(arquivo);
            if (tipo == null || !tipo.contains("pdf")) {
                baos.write(buffer);
            }
            em.persist(arquivoParte);
        }
        if (tipo == null || !tipo.contains("pdf")) {
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ByteArrayInputStream bais2 = new ByteArrayInputStream(baos.toByteArray());
            BufferedImage bi, b2 = null;
            bi = ImageIO.read(bais);
            b2 = ImageIO.read(bais2);
            b2 = redimensionaParaWeb(b2);
            bi = redimensiona(bi);
            baos = new ByteArrayOutputStream();
            ImageIO.write(bi, "jpg", baos);
            arquivo.setThumb(baos.toByteArray());
            baos = new ByteArrayOutputStream();
            ImageIO.write(b2, "jpg", baos);
            arquivo.setWebthumb(baos.toByteArray());
        }
        em.persist(arquivo);

    }

    public static Arquivo recupera(Long id) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        return em.find(Arquivo.class, id);
    }

    public static String getCaminhoArquivos() {
        if (Util.getOs().contains("Windows")) {
            return "c:\\dadosarquivos";
        }
        return "/dadosarquivos";
    }

    public static InputStream recuperaArquivoDisco(String subPasta, Long id) {
        try {
            String nomePasta = getCaminhoArquivos() + "/" + subPasta;
            String nomeArquivo = nomePasta + "/" + id;
            FileInputStream fis = new FileInputStream(nomeArquivo);
            return fis;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void armazenaArquivoDisco(String subPasta, Long id, InputStream is) throws Exception {
        String nomePasta = getCaminhoArquivos() + "/" + subPasta;
        String nomeArquivo = nomePasta + "/" + id;
        File pasta = new File(nomePasta);
        if (!pasta.exists()) {
            pasta.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream(nomeArquivo);
        int t = 0;
        while ((t = is.available()) > 0) {
            byte buffer[];
            if (t > 4096) {
                buffer = new byte[4096];
            } else {
                buffer = new byte[t];
            }
            if (buffer.length != is.read(buffer)) {
                throw new RuntimeException("Escrito com erro!");
            }
            fos.write(buffer);
        }
        fos.close();
    }

    public static BufferedImage redimensiona(BufferedImage bi) {
        int max_height = 150;
        int max_width = 150;
        BufferedImage thumb = new BufferedImage(max_width, max_height, BufferedImage.TYPE_INT_RGB);
        int height = bi.getHeight();
        int width = bi.getWidth();
        double ratio = ((double) width) / height;
        if (width > height) {
            width = max_width;
            height = (int) (max_height / ratio);
        } else {
            height = max_height;
            width = (int) (max_width * ratio);
        }

        thumb.getGraphics().setColor(Color.WHITE);
        thumb.getGraphics().fillRect(0, 0, max_width, max_height);

        thumb.getGraphics().drawImage(bi, max_width / 2 - width / 2, max_height / 2 - height / 2, width, height, null);

        return thumb;
    }

    public static BufferedImage redimensionaParaWeb(BufferedImage bi) {
        int max_height = 623;
        int max_width = 623;
        BufferedImage thumb = new BufferedImage(max_width, max_height, BufferedImage.TYPE_INT_RGB);
        int height = bi.getHeight();
        int width = bi.getWidth();
        double ratio = ((double) width) / height;
        if (width > height) {
            width = max_width;
            height = (int) (max_height / ratio);
        } else {
            height = max_height;
            width = (int) (max_width * ratio);
        }

        thumb.getGraphics().setColor(Color.WHITE);
        thumb.getGraphics().fillRect(0, 0, max_width, max_height);

        thumb.getGraphics().drawImage(bi, max_width / 2 - width / 2, max_height / 2 - height / 2, width, height, null);

        return thumb;
    }

    public static boolean buscaImagemUsada(Arquivo arquivo) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();

        Query q = em.createQuery("from Produto obj where obj.arquivo = :arq");
        q.setParameter("arq", arquivo);
        q.setMaxResults(1);
        if (!q.getResultList().isEmpty()) {
            return true;
        }

        Query q3 = em.createQuery("from GrupoDeProdutos obj where obj.arquivo = :arq");
        q3.setParameter("arq", arquivo);
        q3.setMaxResults(1);
        if (!q3.getResultList().isEmpty()) {
            return true;
        }

        Query q2 = em.createQuery("from Estabelecimento obj where obj.arquivo = :arq");
        q2.setParameter("arq", arquivo);
        q2.setMaxResults(1);
        if (!q2.getResultList().isEmpty()) {
            return true;
        }

        Query q4 = em.createQuery("from Idioma obj where obj.arquivo = :arq");
        q4.setParameter("arq", arquivo);
        q4.setMaxResults(1);
        if (!q4.getResultList().isEmpty()) {
            return true;
        }

        Query q5 = em.createQuery("from PracaDeAlimentacao obj where obj.arquivo = :arq");
        q5.setParameter("arq", arquivo);
        q5.setMaxResults(1);
        if (!q5.getResultList().isEmpty()) {
            return true;
        }

        return false;
    }

}

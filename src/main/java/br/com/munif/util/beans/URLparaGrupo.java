package br.com.munif.util.beans;

import br.com.munif.entidades.Grupo;
import br.com.munif.entidades.Usuario;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author munif
 */
public class URLparaGrupo {

    private static final List<OperationExpression> operations = Arrays.asList(

    );

    public static boolean getGrupo(String url, Usuario usu) {
        for (OperationExpression oe : operations) {
            if (url.matches(oe.url)) {
                if (usu.temEsseGrupo(oe.grupo)) {
                    return true;
                }
            }
        }
        return false;
    }
}

class OperationExpression {

    public String url;
    public Grupo grupo;

    public OperationExpression(Grupo grupo, String url) {
        this.grupo = grupo;
        this.url = url;
    }
}

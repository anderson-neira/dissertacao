package br.com.munif.util.beans;

import br.com.munif.entidades.EntidadePadrao;
import br.com.munif.util.Persistencia;
import br.com.munif.util.web.MuniFilter;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author munif
 */
public class GenericCrudBean {

    public static final int MAXCONSULTA = 1000;
    private static GenericCrudBean instance = new GenericCrudBean();

    public static GenericCrudBean getInstance() {
        return instance;
    }

    private GenericCrudBean() {
    }

    public Object recuperar(Class clazz, Object chave) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        return em.find(clazz, chave.toString());
    }
//    
//    public Object recuperarPorUUID(Class clazz, UUID chave) {
//        EntityManager em = MuniFilter.getEntityManagerCorrente();
//        return em.find(clazz, chave);
//    }

    public void insereOuAltera(EntidadePadrao entidade) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        if (entidade.isNovo()) {
            em.persist(entidade);
        } else {
            em.merge(entidade);
        }
    }

    public void inserir(EntidadePadrao entidade) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        em.persist(entidade);
    }

    public void alterar(EntidadePadrao entidade) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        em.merge(entidade);
    }

    public void remover(EntidadePadrao entidade) {
        if (entidade == null || entidade.isNovo()) {
            return;
        }
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        entidade = (EntidadePadrao) recuperar(entidade.getClass(), entidade.getId());
        em.remove(entidade);
    }

    public List listarTodos(Class classe) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from " + classe.getCanonicalName() + " order by " + Persistencia.primeiroAtributo(classe).getName());
        return q.getResultList();
    }

    public List listarTodos(Class classe, String ordenadoPor) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from " + classe.getCanonicalName() + " order by " + ordenadoPor);
        return q.getResultList();
    }

    public List listarTodos(Class classe, String ordenadoPor, String criterio) {
        EntityManager em = MuniFilter.getEntityManagerCorrente();
        Query q = em.createQuery("from " + classe.getCanonicalName() + " obj where " + criterio + " order by obj." + ordenadoPor);
        return q.getResultList();
    }

    public void persistirItensDaColecao(List<EntidadePadrao> colecao) {
        for (EntidadePadrao obj : colecao) {
            if (obj.isNovo()) {
                inserir(obj);
            } else {
                alterar(obj);
            }
        }
    }

    public void removeColecao(Collection<EntidadePadrao> colecao) {
        for (EntidadePadrao obj : colecao) {
            remover(obj);
        }
    }
}

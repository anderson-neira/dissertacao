/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.beans;

import br.com.munif.entidades.Grupo;
import br.com.munif.entidades.GrupoUsuario;
import br.com.munif.entidades.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author munif
 */
public class Seed {

    public static void insereDadosPadrao(EntityManager em) {
        insereUsuarioPadrao(em);
    }

    private static void insereUsuarioPadrao(EntityManager em) {
        List<GrupoUsuario> l = em.createQuery("from GrupoUsuario gu where gu.grupo='ADMIN'").getResultList();
        System.out.println("VERIFICANDO ADMIN " + l.size());
        if (l.isEmpty()) {
            System.out.println("SISTEMA SEM ADMIN, CRIANDO USUARIO PADRAO");
            Usuario usu = new Usuario();
            usu.setAtivo(Boolean.TRUE);
            usu.setLogin("admin");
            usu.setSenha("qwe123");
            GrupoUsuario gu = new GrupoUsuario();
            gu.setUsuario(usu);
            gu.setGrupo(Grupo.ADMIN);
            em.persist(usu);
            em.persist(gu);
        }
    }

}

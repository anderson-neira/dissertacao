package br.com.munif.util.web;


import java.util.List;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author munif
 */
@SessionScoped
@ManagedBean
public class TraduzJSF {

    /**
     * TRADUZIR UMA UNICA MENSAGEM COM UMA UNICA CHAVE PARA O BEAN PASSAR UMA
     * CHAVE PARA O METODO PARA QUE ELE RETORNE O VALOR CORRESPONDENTE A
     * LINGUAGEM DO NAVEGADOR
     */
    public static String getMsgsTraduzidas(String key) {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("languages.sistema", FacesUtil.localeAtual());
            return bundle.getString(key);
        } catch (Exception e) {
            return key.replaceAll("_", " ");
        }
    }

    /**
     * TRADUZIR UMA UNICA MENSAGEM COM VARIAS CHAVES PARA O BEAN, NESTE CASO É
     * NECESSARIO PASSAR PARA O METODO UM ARRAY DE CHAVE(STRING) PARA QUE ESTE
     * METODO PASSA ACHAR TODAS AS CHAVES E RETORNA-LAS EM
     * SEQUENCIA(CONCATENADO) EXEMPLO DE ARRAY VALIDO PARA STRING, PARA NÃO SER
     * NECESSARIO INSTACIAR UMA NOVA LISTA E ADD AS CHAVES NELA
     * Arrays.asList("personalizacao_inserido", "data_inicial")
     */
    public static String getMsgsTraduzidas(List<String> keys) {
        String msg = "";
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("languages.sistema", FacesUtil.localeAtual());
            for (String key : keys) {
                msg += bundle.getString(key);
            }
        } catch (Exception e) {
            return keys.get(0).replaceAll("_", " ");
        }
        return msg;
    }

//    public static Idioma acharIdioma() {
//        return IdiomaBean.listaFiltrandoTipo(FacesUtil.retornaIdiomaAtual());
//    }

    /**
//     *
//     * METODOS PARA TRADUZIR NOME DE PERSONALIZAÇÃO, NOME DO PRODUTO DESCRIÇÃO
//     * DO PRODUTO E GRUPO DO PRODUTO, ESTES METODOS ESTÃO SENDO UTILIZADOS PARA
//     * FAZER A "INTELIGENCIA" DAS TAGS JSP, E TAMBÉM PARA TRADUZIR AS LISTAS DE
//     * CORRESPONDENTES
//     *
//     */
//    public static String traduzirPersonalizacao(Personalizacao personalizacao) {
//        //EVITAR NULL POINTER
//        if (personalizacao == null) {
//            return "";
//        }
//        if (personalizacao.isNovo()) {
//            return "";
//        }
//        // ACHAR O IDIOMA
//        Idioma idioma = acharIdioma();
//        //TRADUZIR
//        return TraduzirBean.traduzirPersonalizacaoGenerico(personalizacao, idioma);
//    }
//
//    public static String traduzirNomeProduto(Produto produto) {
//        if (produto == null) {
//            return "";
//        }
//        if (produto.isNovo()) {
//            return "";
//        }
//        Idioma idioma = acharIdioma();
//        return TraduzirBean.traduzirProdutoNomeGenerico(produto, idioma);
//    }
//
//    public static String traduzirDescricaoProduto(Produto produto) {
//        if (produto == null) {
//            return "";
//        }
//        if (produto.isNovo()) {
//            return "";
//        }
//        Idioma idioma = acharIdioma();
//        return TraduzirBean.traduzirProdutoDescricaoGenerico(produto, idioma);
//    }
//
//    public static String traduzirGrupo(GrupoDeProdutos grupo) {
//        if (grupo == null) {
//            return "";
//        }
//        if (grupo.isNovo()) {
//            return "";
//        }
//        Idioma idioma = acharIdioma();
//        return TraduzirBean.traduzirGrupo(grupo, idioma);
//    }
//
//    public static String traduzirTipoAvaliacao(TipoAvaliacoes tipoAvaliacoes) {
//        //EVITAR NULL POINTER
//        if (tipoAvaliacoes == null) {
//            return "";
//        }
//        if (tipoAvaliacoes.isNovo()) {
//            return "";
//        }
//        // ACHAR O IDIOMA
//        Idioma idioma = acharIdioma();
//        //TRADUZIR
//        return TraduzirBean.traduzirTipoAvaliacao(tipoAvaliacoes, idioma);
//    }
//
//    public static String traduzirCategoriaDeEstabelecimento(CategoriaDeEstabelecimento categoria) {
//        //EVITAR NULL POINTER
//        if (categoria == null) {
//            return "";
//        }
//        if (categoria.isNovo()) {
//            return "";
//        }
//        // ACHAR O IDIOMA
//        Idioma idioma = acharIdioma();
//        //TRADUZIR
//        return TraduzirBean.traduzirCategoria(categoria, idioma);
//    }
//
//    public static String traduzirTipoAvaliacaoPraca(TipoAvaliacoesPraca tipoAvaliacoes) {
//        //EVITAR NULL POINTER
//        if (tipoAvaliacoes == null) {
//            return "";
//        }
//        if (tipoAvaliacoes.isNovo()) {
//            return "";
//        }
//        // ACHAR O IDIOMA
//        Idioma idioma = acharIdioma();
//        //TRADUZIR
//        return TraduzirBean.traduzirTipoAvaliacaoPraca(tipoAvaliacoes, idioma);
//    }
//
//    public static String traduzirTipoPessoa(TipoPessoa tipoPessoa) {
//        //EVITAR NULL POINTER
//        if (tipoPessoa == null) {
//            return "";
//        }
//        if (tipoPessoa.isNovo()) {
//            return "";
//        }
//        // ACHAR O IDIOMA
//        Idioma idioma = acharIdioma();
//        //TRADUZIR
//        return TraduzirBean.traduzirTipoPessoa(tipoPessoa, idioma);
//    }
//    public static String traduzirTipoMesa(TipoDeMesa tipoDeMesa) {
//        //EVITAR NULL POINTER
//        if (tipoDeMesa == null || tipoDeMesa.isNovo()) {
//            return "";
//        }
//
//        // ACHAR O IDIOMA
//        Idioma idioma = acharIdioma();
//        //TRADUZIR
//        return TraduzirBean.traduzirTipoMesa(tipoDeMesa, idioma);
//    }
//
//    
//     public static String traduzirNomeAtendimento(Atendimento atendimento) {
//        if (atendimento == null) {
//            return "";
//        }
//        if (atendimento.isNovo()) {
//            return "";
//        }
//        Idioma idioma = acharIdioma();
//        return TraduzirBean.traduzirAtendimentoDescricaoGenerico(atendimento, idioma);
//    }
//
//    public static String traduzirDescricaoAtendimento(Atendimento atendimento ){
//        if (atendimento == null) {
//            return "";
//        }
//        if (atendimento.isNovo()) {
//            return "";
//        }
//        Idioma idioma = acharIdioma();
//        return TraduzirBean.traduzirAtendimentoDescricaoGenerico(atendimento, idioma);
//    }
}

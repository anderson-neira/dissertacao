package br.com.munif.util.web;

import br.com.munif.entidades.AuditoriaRevisao;
import br.com.munif.util.Persistencia;
import org.hibernate.envers.RevisionListener;

public class OuvinteAuditoria implements RevisionListener {

    @Override
    public void newRevision(Object o) {
        AuditoriaRevisao ar = (AuditoriaRevisao) o;
        ar.setUsuario(MuniFilter.getTlda().getUsuario());
        ar.setIp(MuniFilter.getTlda().getIp());
    }
}

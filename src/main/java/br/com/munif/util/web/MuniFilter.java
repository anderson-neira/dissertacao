/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.web;

import br.com.munif.util.beans.URLparaGrupo;
import br.com.munif.util.beans.Seed;
import br.com.munif.entidades.Usuario;
import br.com.munif.util.Constantes;
import java.io.IOException;
import java.util.Enumeration;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author munif
 */
@WebFilter(filterName = "MuniFilter",
        servletNames = {"Faces Servlet"},
        urlPatterns = {"/thumb/*", "/arquivo/*", "/api/*", "/webthumb/*", "/index.html"})
public class MuniFilter implements Filter {

    private static ThreadLocal<EntityManager> tlem = new ThreadLocal<>();
    private static ThreadLocal<DadosAuditoria> tlda = new ThreadLocal<>();

    public static DadosAuditoria getTlda() {
        return tlda.get();
    }

    public static void setTlda(ThreadLocal<DadosAuditoria> tlda) {
        MuniFilter.tlda = tlda;
    }

    private static EntityManagerFactory emf;
    

    public static EntityManager getEntityManagerCorrente() {
        return tlem.get();
    }

    public static EntityManagerFactory getEmf() {
        return emf;
    }

    public static void setEmf(EntityManagerFactory emf1) {
        emf = emf1;
    }

    public static void setEntityManagerCorrente(EntityManager em) { // para funcionar o testa 
        tlem.set(em);
    }

    @Override
    public void init(FilterConfig fc) throws ServletException {
        if (Constantes.debug) {
            System.out.println("Iniciando filtro MuniFilter");
        }
        emf = Persistence.createEntityManagerFactory("UnidadeDePersistencia");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            DadosAuditoria da = new DadosAuditoria("system", "system");
            tlda.set(da);
            Seed.insereDadosPadrao(em);
            em.getTransaction().commit();
        } catch (Exception ex) {
            //ex.printStackTrace();
            if (em.isOpen()) {
                em.getTransaction().rollback();
            }
        }
        em.close();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest hst = (HttpServletRequest) request;
        HttpServletResponse hsr = (HttpServletResponse) response;
        StringBuffer requestURL = hst.getRequestURL();
        if (ehEstatica(requestURL)) {
            chain.doFilter(request, response);
            return;
        }
        Usuario usu = (Usuario) hst.getSession(true).getAttribute("usuario");
        DadosAuditoria da = new DadosAuditoria();
        da.setIp(hst.getRemoteAddr());
        da.setUsuario(usu != null ? usu.getLogin() : "nao logado");
        tlda.set(da);

        Enumeration<String> a = request.getAttributeNames();
        EntityManager em = null;
        try {

            em = emf.createEntityManager();
            tlem.set(em);
            em.getTransaction().begin();
            chain.doFilter(request, response);
            if (em.getTransaction().getRollbackOnly()) {
                if (Constantes.debug) {
                    System.out.println("--------------> ROOLBACK");
                }
                em.getTransaction().rollback();
            } else {
                em.getTransaction().commit();
            }

        } catch (Throwable ex) {
            if (em != null && em.getTransaction().isActive()) {
                em.getTransaction().rollback();
                if (Constantes.debug) {
                    System.out.println("--------------> ROOLBACK EXCEPTION");
                }
                ex.printStackTrace();
            }
        } finally {
            if (em != null) {
                em.close();
                tlem.remove();
            }
        }
    }

    @Override
    public void destroy() {
        if (Constantes.debug) {
            System.out.println("Encerrando MuniFilter");
        }
        try {
            emf.close();
        } catch (Exception e) {
        }
    }
    private static final String extensoesEstaticas[] = {".css", ".js", ".jpeg", ".jpg", ".png", ".gif", ".html", ".svg"};

    private boolean ehEstatica(StringBuffer requestURL) {
        String url = requestURL.toString().toLowerCase();
        if (url.contains("javax.faces.resource")) {
            return true;
        }
        for (String ext : extensoesEstaticas) {
            if (url.endsWith(ext)) {
                return true;
            }
        }
        return false;
    }

}

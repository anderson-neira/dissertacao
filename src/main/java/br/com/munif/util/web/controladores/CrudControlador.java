package br.com.munif.util.web.controladores;

import br.com.munif.entidades.EntidadePadrao;
import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.FacesUtil;
import br.com.munif.util.web.TraduzJSF;
import java.util.List;

/**
 *
 * @author munif
 */
public abstract class CrudControlador<T extends EntidadePadrao> {

    protected T entidade;
    protected List<T> lista;
    protected String filtro;
    protected boolean temMais = false;
    protected boolean continuarInserindo = false;
    protected String mensagem;
    protected Class classeEntidade;

    public CrudControlador(Class<T> classeDaEntidade) {
        this.classeEntidade = classeDaEntidade;
        lista = listaTodos();
        verificaTemMais();
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isContinuarInserindo() {
        return continuarInserindo;
    }

    public void setContinuarInserindo(boolean continuarInserindo) {
        this.continuarInserindo = continuarInserindo;
    }

    public abstract List<T> listaTodos();

    public abstract List<T> listaTodosFiltrando();

    public void novo() {
        try {
            entidade = (T) classeEntidade.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    public boolean isTemMais() {
        return temMais;
    }

    public T getEntidade() {
        return entidade;
    }

    public void setEntidade(T entidade) {
        continuarInserindo = false;
        this.entidade = entidade;
    }

    public List<T> getLista() {
        if (lista == null) {
            lista = listaTodos();
            verificaTemMais();
        }
        return lista;
    }

    public void filtrar() {
        if (filtro != null) {
            lista = listaTodosFiltrando();
            verificaTemMais();
        }
    }

    protected void verificaTemMais() {
        if (lista.size() > GenericCrudBean.MAXCONSULTA) {
            temMais = true;
            lista.remove(GenericCrudBean.MAXCONSULTA);
        } else {
            temMais = false;
        }
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public void excluir(T aRemover) {
        GenericCrudBean.getInstance().remover(aRemover);
        FacesUtil.addMessageInfo("msgs", TraduzJSF.getMsgsTraduzidas(classeEntidade.getSimpleName() + "_excluido"), "");
        lista = null;
    }

    public String salvar() {
        if (entidade.isNovo()) {
            GenericCrudBean.getInstance().inserir(entidade);
            FacesUtil.addMessageInfo("msgs", TraduzJSF.getMsgsTraduzidas(classeEntidade.getSimpleName() + "_inserido"), "");
        } else {   
            GenericCrudBean.getInstance().alterar(entidade);
            FacesUtil.addMessageInfo("msgs", TraduzJSF.getMsgsTraduzidas(classeEntidade.getSimpleName() + "_alterado"), "");
        }
        lista = listaTodos();
        verificaTemMais();
        if (continuarInserindo) {
            novo();
        }
        return continuarInserindo ? "edita" : "lista";
    }

    public void cancelar() {
        entidade = null;
        FacesUtil.addMessageWarn("msgs", TraduzJSF.getMsgsTraduzidas("operacao_cancelada"), "");
    }

}

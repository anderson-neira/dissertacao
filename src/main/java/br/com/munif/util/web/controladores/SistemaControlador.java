/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.web.controladores;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.web.ConverterGenerico;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.convert.Converter;

/**
 *
 * @author Munif
 */
@ManagedBean
public class SistemaControlador {
    
    private String buscaMenu="";

    public String getBuscaMenu() {
        return buscaMenu;
    }

    public void setBuscaMenu(String buscaMenu) {
        this.buscaMenu = buscaMenu;
    }
    
    

    public Converter converter(String classe) {
        try {
            return new ConverterGenerico(Class.forName(classe));
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List lista(String classe) {
        try {
            return GenericCrudBean.getInstance().listarTodos(Class.forName(classe));
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List lista(String classe, String ordernadoPor) {
        try {
            return GenericCrudBean.getInstance().listarTodos(Class.forName(classe), ordernadoPor);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List lista(String classe, String ordernadoPor,String criterio) {
        try {
            return GenericCrudBean.getInstance().listarTodos(Class.forName(classe), ordernadoPor,criterio);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
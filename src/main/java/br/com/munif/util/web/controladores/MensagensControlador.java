package br.com.munif.util.web.controladores;

import br.com.munif.util.web.FacesUtil;
import java.util.Locale;
import javax.faces.context.FacesContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author anderson
 */
public class MensagensControlador {

    private Locale novoLocal;
    public String linguagem;
    public String pais;

    public MensagensControlador() {
//        novoLocal =  EstabelecimentoBean.getByLogin() != null ? EstabelecimentoBean.getByLogin().localeDoIdiomaPreferencial() : 
        novoLocal = FacesUtil.localeAtual();
        linguagem = "";
        pais = "";
    }

    public Locale getNovoLocal() {
        return novoLocal;
    }

    public void setNovoLocal(Locale novoLocal) {
        this.novoLocal = novoLocal;
    }

    public String getLinguagem() {
        return linguagem;
    }

    public void setLinguagem(String linguagem) {
        this.linguagem = linguagem;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String mudarIdioma() {
        if (!"".equals(pais)) {
            this.mudarLocalidade(new Locale(linguagem, pais));
        } else {
            this.mudarLocalidade(new Locale(linguagem));
        }
        return null;
    }

    public void mudarLocalidade(Locale locale) {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    }

    public void mudarIdiomaDoSistema(String novaLinguagem) {
        if (novaLinguagem.contains("_")) { // existe caso como o alemão DE, que não tem o _ (underline/underscore)
            String[] idi = novaLinguagem.split("_");
            linguagem = idi[0];
            pais = idi[1];
            novoLocal = new Locale(idi[0], idi[1]);
        } else {
            novoLocal = new Locale(novaLinguagem);
        }
        mudarLocalidade(novoLocal);
    }

    public String getTimeZone() {
//        if(EstabelecimentoBean.getByLogin() != null){
//            return EstabelecimentoBean.getByLogin().getTimezone()==null?"America/Sao_Paulo":EstabelecimentoBean.getByLogin().getTimezone();
//        } 
        return "America/Sao_Paulo";

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.web.controladores;


import br.com.munif.entidades.Grupo;
import br.com.munif.entidades.GrupoUsuario;
import br.com.munif.entidades.Usuario;
import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.beans.UsuarioBean;
import br.com.munif.util.web.FacesUtil;
import br.com.munif.util.web.MuniFilter;
import br.com.munif.util.web.TraduzJSF;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author munif
 */
@ManagedBean
@SessionScoped
public class UsuarioControlador extends CrudControlador<Usuario> {

    private Usuario usuarioCorrente;
    private String senhaAtual;
    private String senhaNova;
    private String confirmaSenha;
    private List<Usuario> listUsuariosLimitados;
    private Grupo grupo;

    public UsuarioControlador() {
        super(Usuario.class);
        usuarioCorrente = UsuarioBean.recupera(MuniFilter.getTlda().getUsuario());
    }

    @Override
    public List listaTodos() {
        return UsuarioBean.lista();
    }

    @Override
    public List listaTodosFiltrando() {
        return UsuarioBean.listaFiltrando(filtro);
    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(String senhaAtual) {
        this.senhaAtual = senhaAtual;
    }

    public String getSenhaNova() {
        return senhaNova;
    }

    public void setSenhaNova(String senhaNova) {
        this.senhaNova = senhaNova;
    }

    public String getConfirmaSenha() {
        return confirmaSenha;
    }

    public void setConfirmaSenha(String confirmaSenha) {
        this.confirmaSenha = confirmaSenha;
    }

    public Usuario getUsuarioCorrente() {
        return usuarioCorrente;
    }

    public void setUsuarioCorrente(Usuario usuarioCorrente) {
        this.usuarioCorrente = usuarioCorrente;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }


    public void setListUsuariosLimitados(List<Usuario> listUsuariosLimitados) {
        this.listUsuariosLimitados = listUsuariosLimitados;
    }

    public String alteraSenha() {
        if (usuarioCorrente.getSenha().equals(senhaAtual)) {
            if (senhaNova.equals(confirmaSenha)) {
                if (senhaNova.length() > 4) {
                    usuarioCorrente = (Usuario) GenericCrudBean.getInstance().recuperar(Usuario.class, usuarioCorrente.getId());
                    usuarioCorrente.setSenha(senhaNova);
                    return "dashboard";
                } else {
                    mensagem = (TraduzJSF.getMsgsTraduzidas("senhasCurtas"));
                }
            } else {
                mensagem = (TraduzJSF.getMsgsTraduzidas("senhasNaoConferem"));
            }
        } else {
            mensagem = (TraduzJSF.getMsgsTraduzidas("senhaIncorreta"));
        }
        return "trocasenha.xhtml";
    }

    @Override
    public String salvar() {
        if (entidade.isNovo()) { // se o editar for fasle é pq deve-se cadastrar um novo usuario;
            if (UsuarioBean.verificaNomeUsuario(entidade.getLogin())) {
                FacesUtil.addMessageError("msgs", TraduzJSF.getMsgsTraduzidas("usuario_duplicado"), "");
                entidade = new Usuario();
                return "edita";
            }

            GenericCrudBean.getInstance().inserir(entidade);
            GrupoUsuario gu = new GrupoUsuario();
            gu.setGrupo(grupo);
            gu.setUsuario(entidade);
            GenericCrudBean.getInstance().inserir(gu);

         
            FacesUtil.addMessageInfo(TraduzJSF.getMsgsTraduzidas("msgs"), TraduzJSF.getMsgsTraduzidas("usuario_inserido"), "");
        } else { // deve-se editar um usuario já castrado
            FacesUtil.addMessageInfo(TraduzJSF.getMsgsTraduzidas("msgs"), TraduzJSF.getMsgsTraduzidas("usuario_alterado"), "");
            GenericCrudBean.getInstance().alterar(entidade); // só vai alterar a senha do usuario;            
        }
        listUsuariosLimitados = null;
        if (continuarInserindo) {
            novo();
        }
        return continuarInserindo ? "edita" : "lista";
    }

    @Override
    public void filtrar() {
        if (filtro != null) {
//            listUsuariosLimitados = UsuarioBean.listaFiltrandoUsuariosLimitados(filtro);
        }
    }

    public void editarUsuario(Usuario obj) {
        this.entidade = obj;
    }

    @Override
    public void excluir(Usuario aRemover) {
//        Estabelecimento est = EstabelecimentoBean.getByLogin();
//        est.getUsuariosLimitados().remove(aRemover);
        GenericCrudBean.getInstance().remover(aRemover);
        FacesUtil.addMessageInfo("msgs", TraduzJSF.getMsgsTraduzidas(classeEntidade.getSimpleName() + "_excluido"), "");
        lista = null;
        listUsuariosLimitados = null;
    }
}

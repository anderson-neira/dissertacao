/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.web;

import br.com.munif.util.beans.GenericCrudBean;
import br.com.munif.util.Persistencia;
import java.io.Serializable;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Gustavo
 */
public class ConverterGenerico implements Converter, Serializable {

    private Class clazz;

    public ConverterGenerico(Class clazz) {
        this.clazz = clazz;
    }

    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        try {
            Object chave = Persistencia.getFieldId(clazz).getType().getConstructor(String.class).newInstance(value);
            Object obj = GenericCrudBean.getInstance().recuperar(clazz, chave);
            return obj;
        } catch (Exception e) {
            new Exception("Problema ao converter objeto" + e);
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value != null) {
            String aRetornar = String.valueOf(Persistencia.getId(value));
            return aRetornar;
        } else {
            return null;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.web.controladores;

import br.com.munif.entidades.Arquivo;
import br.com.munif.entidades.ArquivoParte;
import br.com.munif.util.beans.ArquivoBean;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author vinicius
 */
@WebServlet(name = "ForneceWebThumb", urlPatterns = {"/webthumb/*"})
public class ForneceWebThumb extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        OutputStream out = response.getOutputStream();
        try {
            ArquivoBean arquivoFacade = ArquivoBean.getInstance();
            if (!request.getParameter("id").equals("")) {
                Long id = Long.parseLong(request.getParameter("id"));
                Arquivo arquivo = arquivoFacade.recupera(id);
                if (arquivo != null) {
                    response.setContentType("image/jpeg");
                    if (arquivo.getWebthumb() != null) {
                         
                        out.write(arquivo.getWebthumb());
                    } else {
                        
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        response.setContentType(arquivo.getMimeType());
                        for (ArquivoParte arquivoParte : arquivo.getPartes()) {
                            baos.write(arquivoParte.getDados());
                        }
                        byte[] buffer = baos.toByteArray();
                        baos.close();
                        ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
                        BufferedImage b2 = ImageIO.read(bais);
                        b2 = ArquivoBean.redimensionaParaWeb(b2);
                        baos = new ByteArrayOutputStream();
                        ImageIO.write(b2, "jpg", baos);
                        arquivo.setWebthumb(baos.toByteArray());
                        out.write(baos.toByteArray());
                    }
                } else {
                    out.write("SEM ARQUIVO".getBytes());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ForneceArquivoControlador.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

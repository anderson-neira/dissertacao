/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.web;

import br.com.munif.util.TipoIdioma;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author peixe
 * @author gecen
 *
 */
public abstract class FacesUtil {

    /**
     *
     */
    private static final String ID_CLIENTE_PADRAO = "msgs";

    /**
     * Adiciona uma mensagem do tipo FacesMessage.SEVERITY_WARN no contexto do
     * JSF, utilizando o id "msgs" padrão.
     *
     * @param summary Mensagem resumida
     *
     * @param detail Detalhe da mensagem
     */
    public static void addMessageWarn(String summary, String detail) {
        addMessageWarn(ID_CLIENTE_PADRAO, summary, detail);
    }

    /**
     * Adiciona uma mensagem do tipo FacesMessage.SEVERITY_WARN no contexto do
     * JSF.
     *
     * @param clientId Nome da variável que será utilizada na view xhtml.
     *
     * @param summary Mensagem resumida
     *
     * @param detail Detalhe da mensagem
     */
    public static void addMessageWarn(String clientId, String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail));
    }

    /**
     * Adiciona uma mensagem do tipo FacesMessage.SEVERITY_ERROR no contexto do
     * JSF, utilizando o id "msgs" padrão.
     *
     * @param summary Mensagem resumida
     *
     * @param detail Detalhe da mensagem
     */
    public static void addMessageError(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(ID_CLIENTE_PADRAO, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
    }

    /**
     * Adiciona uma mensagem do tipo FacesMessage.SEVERITY_ERROR no contexto do
     * JSF.
     *
     * @param clientId Nome da variável que será utilizada na view xhtml.
     *
     * @param summary Mensagem resumida
     *
     * @param detail Detalhe da mensagem
     */
    public static void addMessageError(String clientId, String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
    }

    /**
     * Adiciona uma mensagem do tipo FacesMessage.SEVERITY_INFO no contexto do
     * JSF, utilizando o id "msgs" padrão.
     *
     * @param summary Mensagem resumida
     *
     * @param detail Detalhe da mensagem
     */
    public static void addMessageInfo(String summary, String detail) {
        addMessageInfo(ID_CLIENTE_PADRAO, summary, detail);
    }

    /**
     * Adiciona uma mensagem do tipo FacesMessage.SEVERITY_INFO no contexto do
     * JSF.
     *
     * @param clientId Nome da variável que será utilizada na view xhtml.
     *
     * @param summary Mensagem resumida
     *
     * @param detail Detalhe da mensagem
     */
    public static void addMessageInfo(String clientId, String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail));
    }

    /**
     * @return retorna o idioma atual do sistema
     */
    public static String retornaIdiomaAtual() {
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        if (locale.getCountry().isEmpty()) {
            return locale.getLanguage();
        }
        return locale.getLanguage() + "_" + locale.getCountry();
    }

    /**
     * @return retorna o tipo de idioma atual do sistema
     */
    public static TipoIdioma retornaTipoIdiomaAtual() {
        TipoIdioma tipo = TipoIdioma.valueOf(retornaIdiomaAtual());
        return tipo != null ? tipo : TipoIdioma.pt_BR;
    }

    /**
     * @return retorna o locale Atual do sistema
     */
    public static Locale localeAtual() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getViewRoot().getLocale();
    }

    /**
     * @return retorno o Locale do Brasil
     */
    public static Locale localeBrasil() {
        return new Locale("pt", "BR");
    }

    /**
     * executarJavaScript -> Executa um código javaScript passado por parametro.
     *
     * @param javaScript javaScript a ser executado
     *
     */
    public static void executarJavaScript(String javaScript) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute(javaScript);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util.web;

import br.com.munif.entidades.Grupo;
import br.com.munif.util.beans.UsuarioBean;
import br.com.munif.entidades.Usuario;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author munif
 */
@SessionScoped
@ManagedBean
public class LoginControlador {

    private String mensagem = "";

    private String login = "";
    private String senha = "";

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String loga() {
        Usuario usu = UsuarioBean.recupera(login, senha);
        if (usu != null) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            if (!session.isNew()) {
                session.invalidate();
                session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            }
            session.setAttribute("usuario", usu);
            usu.setLastLogin(new Date());
            if (usu.temEsseGrupo(Grupo.ADMIN)) { // é admin
                return "/dashboardadmin.xhtml?faces-redirect=true";     
            } else if (usu.temEsseGrupo(Grupo.CANDIDATO)) { //é credenciada
                return "/dashboard.xhtml?faces-redirect=true";
            } 
            
        } else {
            mensagem = TraduzJSF.getMsgsTraduzidas("erro_usuario_ou_senha");
        }
        return "login.xhtml?faces-redirect=true";
    }

    public Usuario getUsuarioLogado() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        return (Usuario) session.getAttribute("usuario");
    }

    public void logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.invalidate();
    }

}

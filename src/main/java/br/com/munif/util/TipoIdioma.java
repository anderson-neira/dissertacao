/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.util;

/**
 *
 * @author anderson
 */
public enum TipoIdioma {

    pt_BR("Português (Brasil)"),
    en_US("Inglês (EUA)"),
    es_ES("Espanhol (Espanha)"),
    de_DE("Alemão");

    private String descricao;

    private TipoIdioma(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String retornarConstante(TipoIdioma tipo) {
        return tipo.name();
    }

}
